import React from 'react';
import Navigations from 'navigations';
import {View, Text} from 'react-native';
import {Provider} from 'react-redux';
import {store, persistedStore} from 'redux/storeConfig';
import {PersistGate} from 'redux-persist/integration/react';
import { enableScreens } from 'react-native-screens';

export const App = () => {
  enableScreens()
  return (
    <Provider store={store}>
      <PersistGate persistor={persistedStore}>
      <Navigations />
      </PersistGate>
    </Provider>
    );
};
