const formInitialField = {
    value: '',
    isValid: true,
    errorMessage: '',
  };
  
  export const LoginFormState = {
    email: {
      ...formInitialField,
      constraints: [
        {type: 'required', message: 'Email is Required'},
        {type: 'email', message: 'email incorrect'},
      ],
    },
    password: {
      ...formInitialField,
      constraints: [
        {type: 'required', message: 'Password is Required'},
        {
          type: 'minLength',
          data: {min:8},
          message: 'password must be 8 charactor',
        },
      ],
    },
  };
  
  export const SignupFormState = {
    name: {
      ...formInitialField,
      constraints: [{type: 'required', message: 'Name is Required'}],
    },
    username: {
      ...formInitialField,
      constraints: [{type: 'required', message: 'username is Required'}],
    },
    email: {
      ...formInitialField,
      constraints: [
        {type: 'email', message: 'invalid email'},
        {type: 'required', message: 'Email is Required'},
      ],
    },
    password: {
      ...formInitialField,
      constraints: [
        {
          type: 'minLength',
          data: {min: 8},
          message: 'password must be 8 charactor',
        },
        {type: 'required', message: 'Password is Required'},
      ],
    },
    confirmPassword: {
      ...formInitialField,
      constraints: [
        {
          type: 'minLength',
          data: {min: 8},
          message: 'password must be 8 charactor',
        },
        {type: 'required', message: 'Password is Required'},
      ],
    },
  };
  
  export const ForgetPasswordFormState = {
    email: {
      ...formInitialField,
      constraints: [
        {type: 'required', message: 'Email is Required'},
        {type: 'email', message: 'Provide proper email'},
      ],
    },
  };
  
  export const PasswordFormState = {
    password: {
      ...formInitialField,
      constraints: [
        {type: 'required', message: 'Password is Required'},
        {
          type: 'minLength',
          data: {min:8},
          message: 'You would have to meet requirment of password length which is minimum 8',
        },
      ],
    },
  };