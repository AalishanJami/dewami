import {
    AccessToken,
    LoginManager,
    GraphRequest,
    GraphRequestManager,
  } from 'react-native-fbsdk';
  import {
    GoogleSign,
    statusCodes,
    GoogleSignin,
  } from '@react-native-community/google-signin';
import { Platform } from 'react-native';


export const fbHandler = async () => {
  const token = await AccessToken.getCurrentAccessToken();
  LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'native' : 'NATIVE_ONLY');
  // LoginManager.setLoginBehavior(Platform.OS === 'ios' ? 'web' : 'web_only')
  // LoginManager.setLoginBehavior('WEB_ONLY');
    if (token && token.accessToken) {
        const userInfo = await getFacebookData(token.accessToken);
        console.log("====== FACEBOOK HELPER CLASS ==========");
        console.log('userInfo')
        console.log(userInfo)
        return userInfo
    } else {
      // LoginManager.setLoginBehavior('NATIVE_ONLY');
      const res = await LoginManager.logInWithPermissions([
        'public_profile',
        'email',
      ]);
      console.log(res);
      if (res) {
        // debugger;
        const token = await AccessToken.getCurrentAccessToken();
        console.log("======FACRBOOK TOKEN============")
        console.log(token);

        let userInfo = await getFacebookData(token.accessToken);
        console.log("======ELSE USER INFO BASED ON TOKEN============")
        console.log(userInfo);
        return userInfo
      }
    }
  };
  const getFacebookData = accessToken =>
    new Promise(resolve => {
      const infoRequest = new GraphRequest(
        '/me?fields=email,name,first_name,last_name,picture',
        {accessToken: accessToken},
        (error, result) => {
          if (error) {
            console.log('Error fetching data: ' + error.toString());
            resolve(null);
            console.log(error)
            return;
          }
          resolve(result);
        },
      );
      new GraphRequestManager().addRequest(infoRequest).start()
    });
  // ---------------------------GOOGLE-------------------------------
export const googleSigning = async() => {
    try {
      await GoogleSignin.hasPlayServices();
     const userInfo =  await GoogleSignin.signIn()
        console.log('data.user========')
        console.log(userInfo.user)
      return userInfo.user
    } catch (error) {
      console.log(' i am in catching state right now ')
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('cancelled');
        // return
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('in progress');
        // return
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('play error');
        // return

      } else {
        console.log(error);
        // return
      }
    }
  };