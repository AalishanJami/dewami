import {
  COMPAINES_LIST_STATE,
  JOB_LIST_STATE,
  USER_DRAFTS,
  CURRENT_VIEW_PROG,
  ADDING_FEEDBACK,
} from 'redux/constants';

const initialState = [];

export const getRequestReducer = (state = initialState, action) => {
  switch (action.type) {
    case COMPAINES_LIST_STATE:
      return {...state, listOfCompanies: action.payload.listOfCompanies};
    case JOB_LIST_STATE:
      return {...state, listOfJobs: action.payload.listOfJobs};
    case USER_DRAFTS:
      return {...state, allDrafts: action.payload};
    case CURRENT_VIEW_PROG:
      return {...state, currentViewProg: action.payload};
    case ADDING_FEEDBACK:
      return {...state.currentViewProg, feedback:action.payload.feedback};
    default:
      return state;
  }
};
