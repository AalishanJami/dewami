import React, { useState, useReducer } from 'react';
import {StyleSheet} from 'react-native';
import {sizes,colors} from 'styles/theme';
import {Block} from 'components';
import { useSelector, useDispatch } from 'react-redux';

const Hobbies = props => {
  const {hobbies} = useSelector(state => state.userInfo.userData.profile);
  const dispatch = useDispatch();
  console.log(hobbies);
  const [isWaiting, setIsWAiting] = useState(false);
  const initialState = {};
  const reducer = (state, action) => {
    return state;
  };

  const [state, localDispatch] = useReducer(reducer, initialState);
  const {message, showWaiting} = props;
  return (
    <Block
      style={{borderWidth: 1}}
      margin={[0, 0, sizes.getHeight(4), 0]}
      padding={[sizes.getHeight(2), 0, sizes.getHeight(2), 0]}
    />
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
});

export {Hobbies};
