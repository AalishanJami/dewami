import React from 'react';
import {JobLocation, VisaSelection} from './Section';
import {Text, Block, Button} from 'components';
import {StyleSheet} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {SubHeading} from '../../Section';

const JobLocationComp = props => {
  console.log('******JOBLOCATIONCOMP******');
  console.log(props.state);
  console.log('******JOBLOCATIONCOMP******');

  const {jobLocation, visaType} = props.state;
  const {localDispatch} = props;

  const ADD_JOB_LOC = 'ADD_JOB_LOC';
  const VISA = 'VISA';
  const REMOVE_VISA_TYPE = 'REMOVE_VISA_TYPE';
  const REMOVE_JOB_LOC = 'REMOVE_JOB_LOC';
  const CHANGE_JOB_LOC = 'CHANGE_JOB_LOC';
  // ============================ADD
  const addHandler = (value, index) => {
    const count = 1;
    const newAddedValue = jobLocation?.length + count;
    if (index === 0) {
      console.log('index === 0');
      localDispatch({
        type: ADD_JOB_LOC,
        payload: JSON.stringify(newAddedValue),
        // payload: JSON.stringify(count),
      });
      localDispatch({
        type: VISA,
        payload: JSON.stringify(newAddedValue),
      });
    } else {
      localDispatch({
        type: ADD_JOB_LOC,
        payload: JSON.stringify(count),
      });
      localDispatch({
        type: VISA,
        payload: JSON.stringify(count),
      });
    }
  };
  // ============================REMOVE
  const removeHandler = (value, index) => {
    const newJobFilteredArray = jobLocation.filter((o, i) => i != index);
    localDispatch({type: REMOVE_JOB_LOC, payload: newJobFilteredArray});
    localDispatch({type: REMOVE_VISA_TYPE, payload: newJobFilteredArray});
  };
  // ============================CHANGE
  const locationHandler = (arrayData, itemValue) => {
    localDispatch({type:CHANGE_JOB_LOC, payload:itemValue})
  }
  const visaChange = (itemValue, index) => {
    localDispatch({type: VISA, payload:itemValue});
  }

  return (
    <jobLocation />
  )

};

const styles = StyleSheet.create({
  pickerStyle: {
    width: sizes.getWidth(60),
    transform: [{scaleX: 0.8}, {scaleY: 0.8}],
    height: sizes.getHeight(5),
  },
  recordKeeperCon: {
    height: sizes.getHeight(14),
    width: '100%',
    // backgroundColor: 'red',
    // marginBottom: sizes.getHeight(2),
  },
  singlePro: {
    // borderWidth:1,
    // height: '100%',
    // marginVertical:10,
    borderRadius: sizes.getWidth(1),
    borderStyle: 'dashed',
    // marginBottom:2
  },
  btnStyle: {
    width: sizes.getWidth(11),
    height: '80%',
    borderRadius: sizes.getWidth(1),
    //   backgroundColor: colors.gray,
    borderWidth: 0.6,
    borderColor: colors.customRed,
    borderStyle: 'dashed',
  },
});

export {JobLocationComp};
