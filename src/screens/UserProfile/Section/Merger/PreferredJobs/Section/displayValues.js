import React from 'react'
import { Block, Button, Text } from 'components'
import { sizes, colors } from 'styles/theme'
import { StyleSheet } from 'react-native'

const DisplayValues = props => {
    const {el,onPress} = props
    return (
        <Block
        center
        row
        margin={[5,0,0,0]}
        height={sizes.getHeight(5)}
        // style={{borderWidth:1}}
        >
        <Button
          onPress={onPress}
          center
          middle
          style={styles.addedData}
          activeOpacity={0.3}>
          <Text h4 color={colors.primary} style={{textTransform: 'capitalize'}}>
            {el}
          </Text>
        </Button>
      </Block>
    )
}

const styles = StyleSheet.create({
    addedData: {
      // borderWidth:0.5,
      // borderStyle:'dashed',
      backgroundColor:colors.customRed,
      height: sizes.getHeight(4),
      // width: sizes.getWidth(20),
      borderRadius: sizes.getWidth(1),
      marginHorizontal: sizes.getWidth(1),
      paddingHorizontal:sizes.getWidth(3)
    }
})

export {DisplayValues}
