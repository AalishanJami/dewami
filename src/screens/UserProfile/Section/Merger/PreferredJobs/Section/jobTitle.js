import React, {useRef, forwardRef} from 'react';
import {Block, TextField, Button, Text} from 'components';
import {SubHeading} from 'screens/UserProfile/Section/Section';
import {sizes, colors} from 'styles/theme';
import {StyleSheet, TextInput} from 'react-native';

const JobTitle = React.forwardRef((props, ref) => {
  // console.log('ref-----------------')
  // console.log(ref)
  const {sign, onChangeText, onPress, value, name, showAddBtn,placeholder} = props;
  return (
    <Block
      flex={false}
      margin={[sizes.getHeight(1), 0, 0, 0]}
      row
      center
      style={styles.recordKeeperCon}>
      <SubHeading> Job Title</SubHeading>
      <Block
        flex={5}
        style={{
          borderWidth: 0,
          borderWidth: 0,
          paddingBottom: sizes.getHeight(1),
        }}>
        {/* <TextField
            inputRef={ref}
            name={name}
            value={value}
            onChangeText={onChangeText}
            inputStyling={{
              borderBottomWidth: 1,
              width: '90%',
              margin: 0,
              paddingBottom:10,
            }}
          /> */}

        <TextInput
          ref={ref}
          name={name}
          placeholder={placeholder}
          onChangeText={text => onChangeText({name, text})}
          style={{
            borderBottomWidth: 0.6,
            borderColor: colors.gray,
            width: '98%',
          }}
        />
      </Block>
      {showAddBtn&&
      <Block center middle>
        <Button
          activeOpacity={0.5}
          center
          middle
          onPress={onPress}
          // disabled={disabled}
          style={{
            width: '100%',
            borderRadius: sizes.getWidth(1),
            //   backgroundColor: colors.gray,
            borderWidth: 0.6,
            borderColor: colors.gray,
            borderStyle: 'dashed',
          }}>
          <Text bold h1 color={colors.gray}>
            {sign || '-'}
          </Text>
        </Button>
      </Block>
      }
    </Block>
  );
});

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(6),
    //   borderWidth:1,
  },
});

export {JobTitle};
