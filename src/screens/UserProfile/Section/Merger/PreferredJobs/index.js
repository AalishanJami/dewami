import React, {
  useState,
  useRef,
  forwardRef,
  useReducer,
  useEffect,
} from 'react';
import {Block, Text, TextField, Button} from 'components';
import {StyleSheet, ScrollView, ActivityIndicator, Alert} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {sizes, colors} from 'styles/theme';
import {Heading, SubHeading} from '../../Section';
import {JobTitle, JobLocation, DisplayValues, VisaSelection} from './Section';
import {useSelector, useDispatch} from 'react-redux';
import {AddJobTitle} from 'redux/actions';
import {JobLocationComp} from './jobLocation';

const PreferredJobs = props => {
  const {showWaiting, message} = props;
  let dispatch = useDispatch();

  let userInfo = useSelector(state => state.userInfo.userData);
  let auth = useSelector(state => state.auth.userBasicProfile);
  const {
    profile: {pereferred_job},
  } = useSelector(state => state.userInfo.userData);
  const {id} = useSelector(state => state.auth.userBasicProfile);

  // console.log('userInfo');
  // console.log(userInfo);
  // console.log('userInfo');
  // console.log(auth);
  const JOB_CATE = 'JOB_CATE';
  const ADD_JOB_LOC = 'ADD_JOB_LOC';
  const REMOVE_JOB_LOC = 'REMOVE_JOB_LOC';
  const CHANGE_JOB_LOC = 'CHANGE_JOB_LOC';

  const VISA = 'VISA';
  const REMOVE_VISA_TYPE = 'REMOVE_VISA_TYPE';
  const JOB_LVL = 'JOB_LVL';
  const JOB_IND = 'JOB_IND';
  const JOB_TYPE = 'JOB_TYPE';
  const CURRNCY = 'CURRENCY';
  const NPRIOD = 'NPERIOD';
  const SUMRY = 'SUMMARY';
  const JOB_TITLE = 'JOB_TITLE';
  const SALARY = 'TARGET SALARY';
  const SAVE_BTN_DIS = 'DISABLE_SAVE_BUTTON';
  const TITLE = 'TITLE';
  const REMOVE_JOB_TITLE = 'REMOVE_JOB_TITLE';

  const GV = {
    CNTRY: {
      PAK: {name: 'PAKISTAN', value: '1'},
      QTR: {name: 'QATAR', value: '2'},
    },
    RC: {
      CTZ: {name: 'Citizen', value: '1'},
      RVT: {name: 'Residence Visa (Transferable)', value: '2'},
      RVNT: {name: 'Residence Visa (Non-Transferable)', value: '3'},
      STD: {name: 'Student', value: '4'},
      TV: {name: 'Transit Visa', value: '5'},
      VV: {name: 'Visit Visa', value: '6'},
      NV: {name: 'No Visa', value: '7'},
    },
    //generic values+ initial values
    CATE: {
      MGMNT: {name: 'MANAGEMENT', value: '1'},
      BNK: {name: 'BANK', value: '2'},
      SEC: {name: 'SECURITY', value: '3'},
    },
    lvl: {
      BGNR: {name: 'BEGINNER', value: '1'},
      INTR: {name: 'INTERMEDIATE', value: '2'},
    },
    CRNCY: {
      DLR: {name: '$', value: '1'},
      ERO: {name: '£', value: '2'},
    },
    TYPE: {
      FREE: {name: 'FREELANCER', value: '1'},
      INTRN: {name: 'INTERNSHIP', value: '2'},
      VOLN: {name: 'VOLUNTEER', value: '3'},
    },
    IND: {
      BNK: {name: 'BANKING', value: '1'},
      BKRS: {name: 'BAKERS', value: '2'},
      TYR: {name: 'TYRES', values: '3'},
    },
    NPRD: {
      im: {name: 'IMMEDIATELY', value: '1'},
      mo: {name: 'MORE THAN ONE MONTH', value: '2'},
      mt: {name: 'MORE THAN THREE MOTNHS', value: '3'},
      ms: {name: 'MORE THAN SIX MOTNHs', value: '4'},
      my: {name: 'MORE THAN ONE YEAR', value: '5'},
    },
  };

  // const _title = title => {
  //   try {
  //     return JSON.parse(title)?.length && JSON.parse(title);
  //   } catch (e) {
  //     return title;
  //   }
  // };

  let initialState = {
    disableSaveBtn: true,
    // title:           _title(userInfo.profile.pereferred_job.title),
    title: userInfo.profile.pereferred_job.title || [],
    jobCategory: userInfo.profile.pereferred_job.category_id,
    // jobLocation:      _title(userInfo.profile.pereferred_job.country),
    jobLocation: userInfo.profile.pereferred_job.country || ['1'],
    jobLevel: userInfo.profile.pereferred_job.level,
    jobIndustry: userInfo.profile.pereferred_job.industry_id,
    targetSalary: userInfo.profile.pereferred_job.target_salary,
    targetCurrency: userInfo.profile.pereferred_job.target_currency,
    summary: userInfo.profile.pereferred_job.summary,
    jobType: userInfo.profile.pereferred_job.type,
    // visaType:         _title(userInfo.profile.pereferred_job.visa),
    visaType: userInfo.profile.pereferred_job.visa || ['1'],
    noticePeriod: userInfo.profile.pereferred_job.notice_period,
  };

  const [isWaiting, setIsWaiting] = useState(false);
  const reducer = (state, action) => {
    switch (action.type) {
      case SAVE_BTN_DIS:
        return {...state, disableSaveBtn: action.payload};
      case JOB_TITLE:
        if (state.title.length <= 0) {
          return {...state, title: action.payload};
        } else {
          return {...state, title: [...state.title, action.payload]};
        }
      case REMOVE_JOB_TITLE:
        return {...state, title: action.payload};
      case JOB_CATE:
        return {
          ...state,
          jobCategory: action.payload,
          disableSaveBtn: action.payload === pereferred_job.category_id,
        };
      case ADD_JOB_LOC:
        return {
          ...state,
          jobLocation: [...state.jobLocation, action.payload],
          disableSaveBtn: action.payload === pereferred_job.country,
        };
      case REMOVE_JOB_LOC:
        return {
          ...state,
          jobLocation: action.payload,
          disableSaveBtn: action.payload === pereferred_job.country,
        };
      case CHANGE_JOB_LOC:
        console.log('***changing ');
        console.log(action.payload);
        return {...state, jobLocation: [action.payload]};

      case 'tryWithTwo':
        console.log('action payload try with two');
        console.log(action.payload);
        return {...state, jobLocation: [action.payload]};
      case VISA:
        return {
          ...state,
          visaType: [action.payload],
          disableSaveBtn: action.payload === state.jobType,
        };
      case REMOVE_VISA_TYPE:
        console.log('VISA ACTIIN');
        console.log(action.payload);
        return {...state, visaType: action.payload};

      case JOB_LVL:
        return {
          ...state,
          jobLevel: action.payload,
          disableSaveBtn: action.payload === pereferred_job.level,
        };
      case JOB_IND:
        return {
          ...state,
          jobIndustry: action.payload,
          disableSaveBtn: action.payload === pereferred_job.industry_id,
        };
      case JOB_TYPE:
        return {
          ...state,
          jobType: action.payload,
          disableSaveBtn: action.payload === pereferred_job.type,
        };
      case CURRNCY:
        return {
          ...state,
          targetCurrency: action.payload,
          disableSaveBtn: action.payload === pereferred_job.target_currency,
        };
      case SALARY:
        return {
          ...state,
          targetSalary: action.payload,
          disableSaveBtn: action.payload === pereferred_job.target_salary,
        };
      case SUMRY:
        return {
          ...state,
          summary: action.payload,
          disableSaveBtn: action.payload === pereferred_job.summary,
        };
      case NPRIOD:
        console.log('*****************');
        console.log(action.payload);
        return {
          ...state,
          noticePeriod: action.payload,
          disableSaveBtn: action.payload === pereferred_job.notice_period,
        };
      default:
        console.log('default reducer');
        return state;
    }
  };

  const [state, localDispatch] = useReducer(reducer, initialState);

  const [addJobTitle, setAddJobTitle] = useState(state.title);
  // const [addJobTitle, setAddJobTitle] = useState(null);
  const [jobTitleArray, setJobTitleArray] = useState([]);
  const [summary, setSummary] = useState(pereferred_job.summary);
  const [salary, setSalary] = useState(pereferred_job.target_salary);
  const [data, setData] = useState([]);
  const inputRef = useRef();

  const recordHandler = () => {
    if (jobTitleArray.length <= 0) {
      console.log('send to reducer');
      let newArray = {...state, title: addJobTitle};
      localDispatch({type: JOB_TITLE, payload: addJobTitle});
      console.log('<<<<<<<<<NEw Array>>>>>>>>>');
      console.log(newArray);
      return newArray;
    } else {
      console.log('IF STATE HAS ALREADY API DATA EXSITS');
      console.log('state title is not empty ');
      // console.log(jobTitleArray),
      let exsitingArray = {...state, title: [...state.title, jobTitleArray]};
      localDispatch({type: JOB_TITLE, payload: jobTitleArray});
      console.log('<<<<<<<<<exsitingArray>>>>>>>>>');
      console.log(exsitingArray);
      return exsitingArray;
    }
  };

  // =============JOB ADD HANDLER ==============
  const addJobTitleHandler = async () => {
    setAddJobTitle(null);
    showWaiting(true);
    const currentStatus = recordHandler();
    const currentState = {...currentStatus, id: auth.id};
    const result = await AddJobTitle(currentState, err => {
      message('Error. we cannot move forward. ');
      showWaiting(false);
      localDispatch({type: SAVE_BTN_DIS, payload: true});
      setAddJobTitle(null);
    });
    if (result) {
      dispatch(result);
      setAddJobTitle(null);
      localDispatch({type: SAVE_BTN_DIS, payload: true}), showWaiting(false);
      message('INFORMATION UPDATED');
    }
    inputRef.current?.clear();
  };
  // =============JOB REMOVE HANDLER============
  const removeItem = (filterFrom, itemToRemove) => {
    const newData =
      filterFrom.length && filterFrom.filter((o, i) => o !== itemToRemove);
    return newData;
  };
  const removeReqToApi = async selectedToRemoveItem => {
    // console.log('i am here ')
    showWaiting(true);
    const filteredTitle = removeItem(
      pereferred_job.title,
      selectedToRemoveItem,
    );
    const newArray = {...state, title: filteredTitle, id: auth.id};
    const result = await AddJobTitle(newArray, err => {
      return alert(err), showWaiting(false), setAddJobTitle(null);
    });
    dispatch(result), showWaiting(false);
    setAddJobTitle(filteredTitle);
    localDispatch({type: REMOVE_JOB_TITLE, payload: filteredTitle});
    result && message('Successfully Removed ! ');
    return result;
  };
  const jobRemoveHandler = selectedToRemove => {
    console.log(state.title);
    console.log(selectedToRemove);
    Alert.alert(`Are You Sure `, `${selectedToRemove} going to remove`, [
      {
        text: 'Yes Delete ',
        onPress: () => {
          removeReqToApi(selectedToRemove);
        },
      },
      {text: 'Not Yet'},
    ]);
  };
  // ===============LOCATION ADD HANDLER =============
  const addLocationHandler = (value, index) => {
    const count = 1;
    const newAddedValue = state.jobLocation?.length + count;
    if (index === 0) {
      localDispatch({
        type: ADD_JOB_LOC,
        payload: JSON.stringify(newAddedValue),
      });
      localDispatch({
        type: VISA,
        payload: JSON.stringify(newAddedValue),
      });
    } else {
      localDispatch({
        type: ADD_JOB_LOC,
        payload: JSON.stringify(count),
      });
      localDispatch({
        type: VISA,
        payload: JSON.stringify(count),
      });
    }
  };
  // =================REMOVE LOCATION HANDLER=================
  const removeLocationHandler = (value, index) => {
    const newJobFilteredArray = state.jobLocation.filter((o, i) => i != index);
    localDispatch({type: REMOVE_JOB_LOC, payload: newJobFilteredArray});
    localDispatch({type: REMOVE_VISA_TYPE, payload: newJobFilteredArray});
  };

  // =================CHANGE LOCATION HANDLER=================
  const changeHandler = (arrayData, itemValue) => {
    const indexOfSelectedItem = arrayData.indexOf(itemValue.toString());
    const remainingJobTitle = arrayData.splice(indexOfSelectedItem, 1);
    const newArray = [...remainingJobTitle, itemValue];
    return newArray;
  };

  const changeJobHandler = (itemValue, index) => {
    // const indexOfSelectedItem = state.jobLocation.indexOf(itemValue.toString());
    // const remainingJobTitle = state.jobLocation.splice(indexOfSelectedItem, 1);
    // const newArray = [...remainingJobTitle, itemValue];

    const newArrayCountry = changeHandler(state.jobLocation, itemValue);
    localDispatch({type: CHANGE_JOB_LOC, payload: newArrayCountry});

    // const newArrayVisa = changeHandler(state.visaType, itemValue)
    // localDispatch({type: VISA, payload: newArrayVisa});

    // localDispatch({type: CHANGE_JOB_LOC, payload: itemValue});
  };

  console.log('===================================================');
  console.log(state);
  // state.jobLocation
  console.log('===================================================');

  // =============INPUT--HANDLER===============
  const inputHandler = ({name, text}) => {
    switch (name) {
      case JOB_TITLE:
        return (
          //if state doesn't have length than add new array else add in to existing
          state.title.length
            ? (setJobTitleArray(text),
              // console.log('ADDING IN TO ARRAY NOW'),
              // console.log('NOW CURRENT TITLE STATE IS NOT EMPTY'),
              // to enable + button prev was :   setAddJobTitle([text])
              setAddJobTitle(text))
            : // console.log('WRITING EMPTY')
              setAddJobTitle([text])
        );
      case 'targetSalary':
        return;
      case SUMRY:
        return localDispatch({type: SUMRY, payload: text});
      //  return setSummary(text);
      case SALARY:
        return localDispatch({type: SALARY, payload: text});
      default:
        console.log('default input handler');
        return;
    }
  };
  //=================SAVE INFO =======================
  const saveInfoHandler = async () => {
    const currentState = {...state, id};
    setIsWaiting(true);
    showWaiting(true);
    const result = await AddJobTitle(currentState, err => {
      return (
        alert(err),
        showWaiting(false),
        setIsWaiting(false),
        localDispatch({type: SAVE_BTN_DIS, payload: true})
      );
    });
    result &&
      (console.log('coming results'),
      console.log(result),
      dispatch(result),
      setIsWaiting(false),
      showWaiting(false),
      localDispatch({type: SAVE_BTN_DIS, payload: true}),
      message('INFORMATION UPDATED !'));
  };
  // ==========================================================================================================
  return (
    <Block
      padding={[0, sizes.getWidth(2)]}
      margin={[0, 0, sizes.getHeight(3), 0]}>
      <Block>
        {pereferred_job.title?.length > 0 && (
          <Block margin={[10, 0, 0, 5]} flex={false}>
            <Text h3 color={colors.gray}>
              Click to Remove
            </Text>
          </Block>
        )}
        <ScrollView scrollEnabled={true} horizontal={true}>
          {pereferred_job.title?.length > 0 &&
            pereferred_job.title?.map((el, index) => {
              return (
                <DisplayValues
                  onPress={() => jobRemoveHandler(el)}
                  el={el}
                  key={index}
                />
              );
            })}
        </ScrollView>
      </Block>

      <JobTitle
        placeholder={'Add Job Title Here...'}
        // showAddBtn= {
        //   jobTitleArray.length,addJobTitle.length
        //     // ? true
        //     // : false || addJobTitle?.length >= 1
        //     // ? true
        //     // : false
        // }
        showAddBtn={jobTitleArray?.length > 0 || addJobTitle?.length > 1}
        ref={inputRef}
        name={JOB_TITLE}
        onChangeText={inputHandler}
        onPress={addJobTitleHandler}
        sign={'+'}
      />
      {/* JOB CATE */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Job Catagory</SubHeading>
        <Picker
          selectedValue={state.jobCategory}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_CATE, payload: itemValue})
          }
          // style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.CATE.MGMNT.name} value={GV.CATE.MGMNT.value} />
          <Picker.Item label={GV.CATE.BNK.name} value={GV.CATE.BNK.value} />
          <Picker.Item label={GV.CATE.SEC.name} value={GV.CATE.SEC.value} />
        </Picker>
      </Block>
      {/* =========================JOB LOCATION========================= */}

      {/* <Block>
        {state.jobLocation.map((v, i) => {
          return (
            <Block>
              <JobLocation
                el={v}
                countryChange={(v, i) =>
                  localDispatch({type: CHANGE_JOB_LOC, payload: v})
                }
              />
            </Block>
          );
        })}
      </Block> */}

      <Block  row style={{borderWidth: 0}}>
        <Block>
          <SubHeading>Job Location</SubHeading>
        </Block>
        <Block
          flex={false}
          width={sizes.getWidth(64)}
          style={{
            borderWidth: 0.4,
            borderRadius: sizes.getWidth(1),
            borderStyle: 'dashed',
          }}>
          {state.jobLocation.map((v, i) => {
            return (
              <JobLocation
                el={v}
                countryChange={(v, i) => localDispatch({type: CHANGE_JOB_LOC, payload: v})}
              />
            );
          })}
        </Block>
      </Block>
      <Block  margin={[10,0,0,0]} row style={{borderWidth: 0}}>
        <Block>
          <SubHeading>Visa Type</SubHeading>
        </Block>
        <Block
          flex={false}
          width={sizes.getWidth(64)}
          style={{
            borderWidth: 0.4,
            borderRadius: sizes.getWidth(1),
            borderStyle: 'dashed',
          }}>
          {state.visaType.map((v, i) => {
            return (
              <VisaSelection
                el={v}
                visaChange={(v, i) => localDispatch({type: VISA, payload: v})}
              />
            );
          })}
        </Block>
      </Block>

      {/* =========================JOB LOCATION========================= */}

      {/* JOB LEVEL */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Job Level</SubHeading>
        <Picker
          // selectedValue={userInfo.userInfo?.profile.pereferred_job.jobLevel}
          selectedValue={pereferred_job.level}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_LVL, payload: itemValue})
          }
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.lvl.BGNR.name} value={GV.lvl.BGNR.value} />
          <Picker.Item label={GV.lvl.INTR.name} value={GV.lvl.INTR.value} />
        </Picker>
      </Block>
      {/* JOB INDUSTRY */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Job Industry</SubHeading>
        <Picker
          selectedValue={state.jobIndustry}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_IND, payload: itemValue})
          }
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.IND.BNK.name} value={GV.IND.BNK.value} />
          <Picker.Item label={GV.IND.BKRS.name} value={GV.IND.BKRS.value} />
          <Picker.Item label={GV.IND.TYR.name} value={GV.IND.TYR.values} />
        </Picker>
      </Block>
      {/* JOB TYPE */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Job Type</SubHeading>
        <Picker
          selectedValue={state.jobType}
          onValueChange={(itemValue, index) =>
            localDispatch({type: JOB_TYPE, payload: itemValue})
          }
          // style={{width: sizes.getWidth(60), hieght: sizes.getHeight(5)}}
          style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
          <Picker.Item label={GV.TYPE.FREE.name} value={GV.TYPE.FREE.value} />
          <Picker.Item label={GV.TYPE.INTRN.name} value={GV.TYPE.INTRN.value} />
          <Picker.Item label={GV.TYPE.VOLN.name} value={GV.TYPE.VOLN.value} />
        </Picker>
      </Block>
      {/* TARGET SALARY */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Target Salary</SubHeading>

        <Block
          center
          middle
          style={{borderWidth: 0, paddingBottom: sizes.getHeight(2)}}>
          <TextField
            value={state.targetSalary}
            placeholder={`Prev:${pereferred_job?.target_salary}`}
            name={SALARY}
            keyboardType={'numeric'}
            onChangeText={inputHandler}
            inputStyling={{width: '80%', borderBottomWidth: 1}}
          />
        </Block>

        <Block>
          <Picker
            selectedValue={pereferred_job.target_currency}
            onValueChange={(itemValue, index) =>
              localDispatch({type: CURRNCY, payload: itemValue})
            }
            style={{...styles.pickerStyle, width: sizes.getWidth(22)}}>
            <Picker.Item label={GV.CRNCY.DLR.name} value={GV.CRNCY.DLR.value} />
            <Picker.Item label={GV.CRNCY.ERO.name} value={GV.CRNCY.ERO.value} />
          </Picker>
        </Block>
      </Block>
      {/* summary */}
      <Block
        row
        center
        flex={false}
        style={{
          ...styles.recordKeeperCon,
          marginBottom: 10,
          height: sizes.getHeight(15),
        }}>
        <SubHeading>Personal Summary</SubHeading>
        <Block style={{borderWidth: 0, height: '100%'}}>
          <TextField
            name={SUMRY}
            onChangeText={inputHandler}
            value={state.summary}
            placeholder="summary"
            multilines={true}
            numberOfLines={5}
            textAlignVertical="top"
            inputStyling={{
              borderStyle: 'dashed',
              elevation: 0.3,
              borderWidth: 0.6,
              borderRadius: sizes.getWidth(1),
              width: '100%',
              height: '100%',
            }}
          />
        </Block>
      </Block>
      {/* notice period */}
      <Block row center style={styles.recordKeeperCon}>
        <SubHeading>Notice Period</SubHeading>

        <Block>
          <Picker
            selectedValue={state.noticePeriod}
            onValueChange={(itemValue, index) =>
              localDispatch({type: NPRIOD, payload: itemValue})
            }
            style={{...styles.pickerStyle, width: sizes.getWidth(70)}}>
            <Picker.Item label={GV.NPRD.im.name} value={GV.NPRD.im.value} />
            <Picker.Item label={GV.NPRD.mo.name} value={GV.NPRD.mo.value} />
            <Picker.Item label={GV.NPRD.mt.name} value={GV.NPRD.mt.value} />
            <Picker.Item label={GV.NPRD.ms.name} value={GV.NPRD.ms.value} />
            <Picker.Item label={GV.NPRD.my.name} value={GV.NPRD.my.value} />
          </Picker>
        </Block>
      </Block>
      {/* ------------------------------------ */}

      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        // style={{borderWidth: 1}}
      >
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || state.disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              state.disableSaveBtn || isWaiting
                ? colors.gray
                : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  // pickerStyle: {
  //   width: sizes.getWidth(30),
  //   transform: [{scaleX: 0.8}, {scaleY: 0.8}],
  //   height: sizes.getHeight(5),
  // },
  pickerStyle: {
    width: sizes.getWidth(60),
    transform: [{scaleX: 0.8}, {scaleY: 0.8}],
    height: sizes.getHeight(5),
  },
  addedData: {
    borderWidth: 1,
    height: sizes.getHeight(4),
    width: sizes.getWidth(20),
    borderRadius: sizes.getWidth(1),
    marginHorizontal: sizes.getWidth(1),
    // paddingHorizontal:sizes.getWidth(1)
  },
  recordKeeperCon: {
    height: sizes.getHeight(7),
    // borderWidth:1,
    // marginBottom: sizes.getHeight(2),
  },
  jobLocation: {
    height: sizes.getHeight(10),
    width: '100%',
  },
});

export {PreferredJobs};
