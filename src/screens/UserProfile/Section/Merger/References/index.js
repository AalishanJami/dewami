import React, { useState, useReducer } from 'react';
import {Block, Text, TextField} from 'components';
import {Heading, SubHeading} from '../../Section';
import {sizes, colors} from 'styles/theme';
import {StyleSheet} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

const ReferencesInfo = props => {
  const {references} = useSelector(state => state.userInfo.userData.profile)
  const dispatch = useDispatch()
  console.log(references)
  const [isWaiting,setIsWAiting]= useState(false)
  const initialState ={}
  const reducer = (state,action)=>{
    return state
  }

  const [state, localDispatch] = useReducer(reducer,initialState)
  const {message,showWaiting} = props
  return (
    <Block padding={[0, 0, sizes.getHeight(3), 0]}>
      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Name</SubHeading>
        <Block>
          <TextField
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              margin: 0,
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Job Title</SubHeading>
       <Block>
       <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
       </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Company Name</SubHeading>
       <Block>
       <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
       </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Email</SubHeading>
        <Block>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
        </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Phone Number</SubHeading>
        <Block>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
        </Block>
      </Block>

      <Block
        flex={false}
        margin={[sizes.getHeight(4), 0]}
        style={{borderBottomWidth: 3, borderBottomColor: colors.customRed}}
      />

      {/* =========SECOND REF */}

      <Heading> Second Refernece </Heading>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Name</SubHeading>
      <Block>
      <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
      </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Job Title</SubHeading>
        <Block>
        <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
        </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Company Name</SubHeading>
       <Block>
       <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
       </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Email</SubHeading>
       <Block>
       <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
       </Block>
      </Block>

      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <SubHeading>Phone Number</SubHeading>
       <Block>
       <TextField
          inputStyling={{
            borderBottomWidth: 1,
            width: '100%',
            margin: 0,
            paddingBottom: 0,
          }}
        />
       </Block>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
});

export {ReferencesInfo};
