import React, {useReducer, useState, useEffect, useRef} from 'react';
import {Block, Text, TextField, Button, TextFieldOne} from 'components';
import {SubHeading} from '../../Section';
import {sizes, colors} from 'styles/theme';
import {StyleSheet} from 'react-native';
import {Picker} from '@react-native-community/picker';
import {useDispatch, useSelector} from 'react-redux';
import {AddEducation} from 'redux/actions';

const EducationalInfo = props => {
  const {showWaiting, message} = props;
  const auth = useSelector(state => state.auth.userBasicProfile);
  const dispatch = useDispatch();
  const GV = {
    PAK: {name: 'PAKISTAN', value: '1'},
    QATAR: {name: 'QATAR', value: '2'},
    year: [
      '2009',
      '2010',
      '2011',
      '2012',
      '2014',
      '2015',
      '2016',
      '2017',
      '2018',
      '2019',
      '2020',
    ],
    month: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
  };
  
  const UNI_NAME = 'UNIVERSITY_NAME';
  const DEGREE = 'DEGREE';
  const COUNTRY = 'COUNTRY';
  const MAJOR = 'MAJOR';
  const SUMMARY = 'SUMMARY';
  const YEAR = 'YEAR';
  const MONTH = 'MONTH';
  
  const nameRef = useRef()
  const degreeRef = useRef()
  const majorRef = useRef()
  const summaryRef = useRef()
  const [isWaiting, setIsWaiting] = useState(false);
  const initailState = {
    // disableSaveBtn:true,
    uniName: '',
    degree: '',
    country: GV.PAK.value,
    major: '',
    summary: '',
    year: GV.year[5],
    month: GV.month[2],
  };

  const [disableSaveBtn, setDisableSaveBtn] = useState(true);
  const reducer = (state, action) => {
    switch (action.type) {
      case UNI_NAME:
        return {...state, uniName: action.payload};
      case DEGREE:
        return {...state, degree: action.payload};
      case MAJOR:
        return {...state, major: action.payload};
      case MONTH:
        return {...state, month: action.payload};
      case YEAR:
        return {...state, year:action.payload};
      case COUNTRY:
        return {...state, country: action.payload};
      case SUMMARY:
        return {...state, summary: action.payload};
      default:
        return state;
    }
  };
  const [state, localDispatch] = useReducer(reducer, initailState);
  // ======================SAVE
  const saveInfoHandler = async () => {
    showWaiting(true);
    const currentState = {...state, id: auth.id.toString()};
    const result = await AddEducation(currentState, err => {
      alert(err), showWaiting(false);
    });
    console.log('RESULT ==========');
    console.log(result);
    dispatch(result)
    nameRef?.current.clear()
    degreeRef?.current.clear()
    majorRef?.current.clear()
    summaryRef?.current.clear()
    showWaiting(false);
    message('INFORMATION UPDATED')
  };

  useEffect(() => {
    if (
      state.uniName !== '' &&
      state.degree !== '' &&
      state.major !== '' &&
      state.summary !== ''
    ) {
      setDisableSaveBtn(false);
    } else {
      setDisableSaveBtn(true);
    }
  });

  const inputHandler = ({name, text}) => {
    switch (name) {
      case UNI_NAME:
        return localDispatch({type: UNI_NAME, payload: text});
      case DEGREE:
        return localDispatch({type: DEGREE, payload: text});
      case MAJOR:
        return localDispatch({type: MAJOR, payload: text});
      case SUMMARY:
        return localDispatch({type: SUMMARY, payload: text});
      default:
        console.log('default');
        return;
    }
  };

  console.log('===============================');
  console.log(state);
  console.log('===============================');

  return (
    <Block
      padding={[0, sizes.getWidth(2)]}
      margin={[0, 0, sizes.getHeight(4), 0]}>
      <Block
        center
        margin={[sizes.getHeight(2), 0, 0, 0]}
        flex={false}
        row
        center
        style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>University Name</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <TextFieldOne
            ref={nameRef}
            onChangeText={inputHandler}
            name={UNI_NAME}
            inputStyling={{
              borderBottomWidth: 1,
              borderBottomColor: colors.gray2,
              width: '100%',
              margin: 0,
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Degree</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <TextFieldOne
            ref={degreeRef}
            name={DEGREE}
            onChangeText={inputHandler}
            inputStyling={{
              borderBottomWidth: 1,
              borderBottomColor: colors.gray2,
              width: '100%',
              margin: 0,
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Country</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
            selectedValue={state.country}
            onValueChange={(itemValue, index) =>
              localDispatch({type: COUNTRY, payload: itemValue})
            }
            style={styles.pickerStyle}>
            <Picker.Item label={GV.PAK.name} value={GV.PAK.value} />
            <Picker.Item label={GV.QATAR.name} value={GV.QATAR.value} />
          </Picker>
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Graduate Year</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
            selectedValue={state.year}
            onValueChange={(itemValue, index) =>
              localDispatch({type: YEAR, payload: itemValue})
            }
            style={styles.pickerStyle}>
            {GV.year.map(el => {
              return <Picker.Item label={el} value={el} />;
            })}
          </Picker>
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Graduate Month</SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <Picker
            selectedValue={state.month}
            onValueChange={(itemValue, index) =>
              localDispatch({type: MONTH, payload: itemValue})
            }
            style={styles.pickerStyle}>
            {GV.month.map(el => {
              return <Picker.Item label={el} value={el} />;
            })}
          </Picker>
        </Block>
      </Block>

      <Block flex={false} row center style={styles.recordKeeperCon}>
        <Block>
          <SubHeading>Major</SubHeading>
        </Block>

        <Block
          padding={[0, sizes.getWidth(2), sizes.getWidth(2), sizes.getWidth(2)]}
          flex={2}
          style={{borderWidth: 0}}>
          <TextFieldOne
            ref={majorRef}
            name={MAJOR}
            onChangeText={inputHandler}
            inputStyling={{
              borderBottomWidth: 1,
              width: '100%',
              borderBottomColor: colors.gray2,
              margin: 0,
              paddingBottom: 0,
            }}
          />
        </Block>
      </Block>

      <Block
        center
        flex={false}
        style={{
          ...styles.recordKeeperCon,
          marginBottom: 10,
          height: sizes.getHeight(15),
        }}>
        <Block center style={{width: '100%'}}>
          <SubHeading center width={'100%'}>
            Personal Summary
          </SubHeading>
        </Block>
        <Block
          padding={[0, sizes.getWidth(2), 0, 0]}
          style={{
            borderWidth: 0,
            height: '100%',
          }}>
          <TextFieldOne
            ref={summaryRef}
            onChangeText={inputHandler}
            name={SUMMARY}
            onChangeText={inputHandler}
            placeholder="Description"
            multilines={true}
            numberOfLines={5}
            textAlignVertical="top"
            inputStyling={{
              borderWidth: 0.4,
              borderStyle: 'dotted',
              borderRadius: sizes.getWidth(1),
              width: '100%',
              height: '100%',
            }}
          />
        </Block>
      </Block>
      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        padding={[0, sizes.padding]}
        margin={[sizes.getHeight(7), 0, 0, 0]}>
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              disableSaveBtn || isWaiting ? colors.gray : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
  pickerStyle: {
    width: sizes.getWidth(40),
    transform: [{scaleX: 0.9}, {scaleY: 0.9}],
    height: sizes.getHeight(5),
    backgroundColor:'#EEEFEF'
  },
});

export {EducationalInfo};
