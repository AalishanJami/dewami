import React, {useState, useReducer, useEffect} from 'react';
import {Heading} from '../../Section';
import {StyleSheet, ActivityIndicator} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {Block, Button, Text} from 'components';
import {useSelector} from 'react-redux';
import {Picker} from '@react-native-community/picker';

const Skills = () => {
  const {skills} = useSelector(state => state.userInfo.userData.profile);
  // console.log(skills);
  // CONSTANTS
  const Beginner = 'Beginner';
  const Intermediate = 'Intermediate';
  const Expert = 'Expert';
  const Options = [Beginner, Intermediate, Expert];
  const dropDown  = []

  // useEffect(() => {
  //   newDropDown()
  // }, [])


  const getLevel = value => {
    switch (value) {
      case '1':
        return Beginner;
      case '2':
        return Intermediate;
      case '3':
        return Expert;
      default:
        return;
    }
  };
  const [isWaiting, setIsWaiting] = useState(false);
  const initialState = {
    disabledSaveBtn: true,
    skills,
  };

  const reducer = (state, action) => {
    return state;
  };

  const [state, localDispatch] = useReducer(reducer, initialState);

  const saveInfoHandler = () => {};
  return (
    <Block
      style={{borderWidth: 1}}
      margin={[0, 0, sizes.getHeight(4), 0]}
      padding={[sizes.getHeight(2), 0, sizes.getHeight(2), 0]}>
      {/* =============================================CODE=============== */}

      <Block>
        {skills &&
          skills.map((v, i) => {
            return (
              <Block row>
                <Block middle padding={[0, 0, 0, sizes.getWidth(10)]}>
                  <Text h2>{v.name}</Text>
                </Block>
                <Block>
                  <Picker style={styles.pickerStyle}>
                    {/* {Options.map((el) => {
                      console.log(el)
                      return( */}
                          <Picker.Item label={getLevel(v.level)}  />                        
                    {/* //   )
                    // })} */}
                    </Picker>
                </Block>
              </Block>
            );
          })}
      </Block>

      {/* =============================================CODE=============== */}
      <Block
        bottom
        crossRight
        flex={false}
        height={sizes.getHeight(8)}
        padding={[0, sizes.padding]}
        margin={[sizes.getHeight(7), 0, 0, 0]}>
        <Button
          onPress={saveInfoHandler}
          disabled={isWaiting || state.disableSaveBtn}
          activeOpacity={0.6}
          center
          middle
          style={{
            borderRadius: sizes.getWidth(1),
            backgroundColor:
              state.disableSaveBtn || isWaiting
                ? colors.gray
                : colors.customRed,
            width: '40%',
          }}>
          {isWaiting ? (
            <ActivityIndicator size={'large'} color={colors.primary} />
          ) : (
            <Text color={colors.primary}>Save</Text>
          )}
        </Button>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  recordKeeperCon: {
    height: sizes.getHeight(5),
    marginBottom: sizes.getHeight(2),
  },
  pickerStyle: {
    width: sizes.getWidth(40),
    transform: [{scaleX: 0.9}, {scaleY: 0.9}],
    height: sizes.getHeight(5),
    backgroundColor: '#EEEFEF',
  },
});

export {Skills};
