import React from 'react';
import {Block, Text} from 'components';
import {Image} from 'react-native';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';

const SubHeading = props => {
  const {width} = props;
  return (
    <Block
      flex={false}
      //  middle
      //  bottom
      middle
      //    margin={[sizes.getHeight(3),0,0,0]}
      height={sizes.getHeight(5)}
      width={width || sizes.getWidth('25')}
      //  style={{borderWidth:1}}
      style={{
        alignContent: 'center',
      }}>
      <Text h4 bold>
        {props.children}
      </Text>
    </Block>
  );
};

export {SubHeading};
