import React, {useState, useEffect, useRef} from 'react';
import {Block, Text, Button, ActivitySign, CustomAlert} from 'components';
import {sizes, colors} from 'styles/theme';
import {ScrollView, Image} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {useSelector} from 'react-redux';
import {GetUserInfo} from 'redux/actions';
import Toast from 'react-native-easy-toast';

const Profile = ({navigation, route}) => {
  const messageRef = useRef();
  const [isWaiting, setIsWaiting] = useState(false);
  const {userInfo} = useSelector(state => state.userInfo);
  console.log(userInfo);
  const editProfileHandler = () => {
    navigation.navigate('EditProfile');
  };

  useEffect(() => {
    console.log(route.params?.fetchError)
    // route.params?.fetchError &&
    //   messageRef.current?.show(
    //     <CustomAlert
    //       textColor={colors.customRed}
    //       text="Cannot Load Profile. Check your internet connectivity first"
    //     />,
    //     2500,
    //   );
  }, [route.params?.fetchError]);

  return (
    <Block middle>
      <Block
        center
        middle
        flex={false}
        height={sizes.getHeight(25)}
        style={{backgroundColor: colors.customRed, width: '100%'}}>
        <Block
          center
          middle
          flex={false}
          height={sizes.withScreen(0.09)}
          width={sizes.withScreen(0.09)}
          style={{overflow: 'hidden', borderRadius: sizes.withScreen(3)}}>
          {/* <Image source={images.person} style={{resizeMode:'contain', flex:1 }} /> */}
          <Image
            source={icons.avatarDummy}
            style={{resizeMode: 'contain', flex: 1, tintColor: colors.primary}}
          />
          {/* <Image source={{uri:userInfo.avatar}} style={{resizeMode:'contain', flex:1 }} /> */}
        </Block>
        <Block center middle flex={false} margin={[10, 0, 0, 0]}>
          <Text h3 color={colors.primary} style={{textTransform: 'capitalize'}}>
            {/* {userInfo.user.name} */}
          </Text>
          <Text h4 color={colors.primary} style={{textTransform: 'capitalize'}}>
            {/* {userInfo.user.expertise || 'Please Add Expertise'} */}
          </Text>
        </Block>
      </Block>
      <Block padding={[0, sizes.getWidth(13)]}>
        <ScrollView
          style={{width: '100%'}}
          showsVerticalScrollIndicator={false}>
          <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Full Name
            </Text>
            <Text h3 color={colors.gray} style={{textTransform: 'capitalize'}}>
              {/* Ibrahim Muhammad */}
              {/* {userInfo.profile.full_name} */}
            </Text>
          </Block>
          <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Username
            </Text>
            <Text h3 color={colors.gray} style={{textTransform: 'capitalize'}}>
              {/* @username */}
              {/* {userInfo.user.username} */}
            </Text>
          </Block>
          <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Email Address
            </Text>
            <Text h3 color={colors.gray}>
              {/* Ibrahimuhmmad@example.com */}
              {/* {userInfo.user.email} */}
            </Text>
          </Block>
          <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Address
            </Text>
            <Text h3 color={colors.gray}>
              {/* {userInfo.user.phone || '+94723123478'} */}
            </Text>
          </Block>
          <Block middle height={sizes.getHeight(7.5)}>
            <Text h3 bold>
              Industry Expertise
            </Text>
            <Text h3 color={colors.gray}>
              {/* UI & UX Designer */}
              {/* {userInfo.user.expertise || 'No Added Yet'} */}
            </Text>
          </Block>
          <Block middle>
            <Text h3 bold>
              About Me
            </Text>
            <Text h4 style={{lineHeight: 16}} color={colors.gray}>
              {/* There are many variations of passages of Lorem Ipsum available,
              but the majority have suffered alteration in some form, by
              injected humour, or randomised words which don't look even
              slightly believable. If you are going to use a passage of Lorem
              Ipsum, you need to be sure there isn't anything embarrassing
              hidden in the middle of text. All the Lorem Ipsum generators on
              the Internet tend to repeat predefined chunks as necessary, making
              this the first true generator on the Internet. It uses a
              dictionary of over 200 Latin words, combined with a handful of
              model sentence structures, to generate Lorem Ipsum which looks
              reasonable. The generated Lorem Ipsum is therefore always free
              from repetition, injected humour, or non-characteristic words etc. */}
              {/* {userInfo.about_me || 'NULL'} */}
            </Text>
          </Block>
        </ScrollView>
        <Block
          center
          style={{
            position: 'absolute',
            bottom: sizes.getHeight(1),
            width: '100%',
            left: '17%',
          }}
          padding={[0, sizes.getWidth(8)]}
          middle
          height={sizes.getHeight(7.5)}>
          <Button
            activeOpacity={0.6}
            onPress={editProfileHandler}
            center
            middle
            style={{
              backgroundColor: colors.customRed,
              borderRadius: sizes.getWidth(1),
              width: '80%',
              elevation: 5,
              shadowRadius: 3,
              shadowOffset: {
                height: 2,
                width: 0,
              },
              shadowColor: colors.customRed,
              shadowRadius: 10,
            }}>
            <Text h2 color={colors.primary}>
              Edit Profile
            </Text>
          </Button>
        </Block>
      </Block>
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.primary,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
      {isWaiting && <ActivitySign />}
    </Block>
  );
};

export {Profile};
