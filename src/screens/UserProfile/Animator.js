import React, {useState, useEffect, useRef} from 'react';
import {Block, Text} from 'components';
import {TouchableOpacity, Animated, StyleSheet, Image} from 'react-native';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import { SubHeading, Heading } from './Section/Section';
const Animator = props => {
  const [showData, setShowData] = useState(false);
  const {children, title} = props;
  const fadeAnim = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    Animated.timing(fadeAnim, {
      useNativeDriver: true,
      toValue:0.8,
      duration: 1000,
    }).start();
  });
  return (
    <Block style={{elevation:2,marginBottom:sizes.getHeight(1)}}>
      <TouchableOpacity
        activeOpacity={0.6}
        style={styles.headingStyle}
        onPress={() => {setShowData(prev => !prev) }}
        >
        <Heading>{title}</Heading>
        <Image
          source={icons.listIndicatorIcon}
          style={{
            resizeMode: 'contain',
            width: sizes.getWidth(12),
            tintColor: colors.customRed,
            transform:[{rotate:'90deg'}]
          }}
        />
      </TouchableOpacity>
      {showData && (
        <Animated.View style={{opacity: fadeAnim, width: '100%'}}>
          {children}
        </Animated.View>
      )}
    </Block>
  );
};

const styles = StyleSheet.create({
  headingStyle: {
    // borderWidth:1,
    height: sizes.getHeight(7),
    // justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: sizes.getWidth(1),
    paddingHorizontal: sizes.getWidth(2),
    backgroundColor: colors.primary,
  },
});

export default Animator;
