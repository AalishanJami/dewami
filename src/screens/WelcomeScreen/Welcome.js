import React from 'react';
import {Block, Text, Button} from 'components';
import * as icons from 'assets/icons';
import {Image} from 'react-native';
import {colors, sizes} from 'styles/theme';

export const WelcomeScreen = ({navigation}) => {
  // console.log()
  return (
    <Block center middle style={{backgroundColor: colors.customRed}}>
      <Block flex={false} middle>
        <Image source={icons.white_logo} style={{resizeMode:'contain', width:sizes.getWidth(47)}} />
      </Block>
      <Block
        flex={false}
        middle
        row
        style={{ width: '25%', justifyContent: 'space-between'}}>
        <Button onPress={() => {
          navigation.navigate('AgainWelcome')
        }} center middle style={{width: '40%'}}>
          <Image source={icons.english} style={{resizeMode:'contain',width:sizes.getWidth('10')}} />
        </Button>
        <Button
         onPress={() => {
          navigation.navigate('AgainWelcome')
          // navigation.setOptions({title:'updated!'})
        }}
        middle center style={{width: '40%'}}>
        <Image source={icons.arabi} style={{resizeMode:'contain',width:sizes.getWidth('10')}} />
        </Button>
      </Block>
    </Block>
  );
};
