import React from 'react';
import {Block, Text} from 'components';
import {colors, sizes} from 'styles/theme';

const Chat = props => {
  const {chatWith} = props;
  return (
    <Block>
      <Block
        center
        middle
        flex={false}
        style={{height: sizes.getHeight(4), backgroundColor: colors.darkBrown}}>
        <Text h3 color={colors.primary}>{chatWith || 'Please Enter Title'}</Text>
      </Block>
    </Block>
  );
};

export default Chat;
