import React, {useEffect, useState, useRef} from 'react';
import {
  Block,
  Text,
  CustomHeader,
  CustomInput,
  SquareButton,
  Button,
  ActivitySign,
  CustomAlert,
} from 'components';
import {colors, sizes} from 'styles/theme';
import * as icons from 'assets/icons';
import {Image, FlatList, ScrollView} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {GetCompaniesList, GetJobsList} from 'redux/actions';
import {} from 'redux/actions/getRequests';
import Toast from 'react-native-easy-toast';

const HomePage = ({route,navigation}) => {
  // console.log(route)
  // console.log('navigation at home ')
  // console.log(navigation)
  const [isLoading, setIsLoading] = useState(false);
  const messageRef = useRef();
  const alertTiming = 3000;

  // navigation.setOptions({
  //   header: () => {
  //     return (
  //       <CustomHeader bg={colors.primary} tint={colors.customRed} />
  //     )
  //   }
  // })
  const data = [
    {key: '1', name: 'Company', color: colors.brown, image: icons.company},
    {key: '2', name: 'Job', color: colors.darkBrown, image: icons.job},
    {key: '3', name: 'CVBuilder', color: colors.darkBrown, image: icons.cv},
    {
      key: '4',
      name: 'Training Catalogue',
      color: colors.brown,
      image: icons.training_catalog,
    },
  ];
  const {listOfCompanies, listOfJobs} = useSelector(state => state.getLists);
  // console.log(listOfCompanies)
  const dispatch = useDispatch();
  const getCompanies = async () => {
    const result = await GetCompaniesList(err => {
      return (
        // console.log('err'), console.log(err),
        messageRef.current?.show(<CustomAlert text={'err'} />, alertTiming),
        setIsLoading(false)
      );
      // return err
    });
    result && console.log('=============================RESULT ');
    // result &&
    return result;
  };
  const getJobs = async () => {
    const result = await GetJobsList(err => {
      console.log('err'),
        console.log(err),
        messageRef.current?.show(<CustomAlert text={err} />, alertTiming);
      setIsLoading(false);
    });
    console.log('=============================RESULT JOBS ');
    return result
  };

  const getRelaventData = async name => {
    // console.log(name)
    setIsLoading(true);
    switch (`${name}`) {
      case 'Company':
        const companies = await getCompanies();
        dispatch(companies), setIsLoading(false);
        navigation.navigate(`${name}`);
        return;
      case 'Job':
        // if(listOfJobs){
        //   navigation.navigate(`${name}`)
        //   setIsLoading(false)
        // }else{
        //   console.log('new reuqest for jobs')
          const Jobs = await getJobs();
          dispatch(Jobs);
          setIsLoading(false)
          navigation.navigate(`${name}`)
        // }
        return;
      case 'CVBuilder':
        // console.log(name)
        status ? 
          navigation.navigate(`${name}`)
        :
        messageRef.current?.show(<CustomAlert text="Login Is Required" />, alertTiming)
        setIsLoading(false);
        return
      case 'Training Catalogue':
        setIsLoading(false);
        return alert('Training CataLog');
      default:
        setIsLoading(false);
        return console.log('ops');
    }
  };

  const authSection = useSelector(state => state.auth);
  const {status} = authSection;
  // useEffect(() => messageRef.current?.show('', 2000));
  useEffect(()=> {
    route.params?.fetchError &&(
      messageRef.current?.show(
        <CustomAlert text="Cannot Load Profile. Check your internet connectivity first" />,3500
      )
    )
  },[route.params?.fetchError])
  return (
    <Block style={{backgroundColor: colors.primary}}>
      {/* search Area */}
      <Block center flex={false}>
        <CustomHeader bg={colors.primary} tint={colors.customRed} navigation={navigation} />
        <Block
          center
          middle
          flex={false}
          height={sizes.getHeight(5)}
        >
          <Text title bold color={colors.customRed}>
            Your Future Job Gateway
          </Text>
        </Block>
        {/* ======================================================== */}
        <Block
          space={'between'}
          flex={false}
          height={sizes.getHeight(26)}
          margin={[sizes.getHeight(1), 0, 0, 0]}
          middle
          //   style={{borderWidth: 1}}
          padding={[
            0,
            sizes.getWidth(4),
            sizes.getHeight(1),
            sizes.getWidth(4),
          ]}>
          <CustomInput
            bw
            ph={sizes.padding}
            placeholderColor={colors.customRed}
            placeholder={'Job Title or Keyword'}
            source={icons.red_briefCase}
          />
          <CustomInput
            ph={sizes.padding}
            bw
            placeholder={'Area'}
            placeholderColor={colors.customRed}
            source={icons.red_location}
          />
          <CustomInput
            ph={sizes.padding}
            bw
            numeric
            placeholder={'Experience'}
            placeholderColor={colors.customRed}
            source={icons.red_exp}
            width={'40%'}
          />

          <Block
            center
            middle
            style={{
              position: 'absolute',
              //   borderWidth: 1,
              height: '60%',
              width: '80%',
              right: 0,
              zIndex: -10,
              bottom: -sizes.getHeight(5),
            }}>
            <Image
              source={icons.background_beautify}
              style={{resizeMode: 'contain', flex: 0.8}}
            />
          </Block>
          <SquareButton
            textColor={colors.primary}
            bgColor={colors.customRed}
            width={sizes.getWidth(30)}
            height={sizes.getHeight(6)}
            style={{
              borderRadius: sizes.withScreen(0.03),
            }}>
            Search
          </SquareButton>
        </Block>
      </Block>

      {/* compant jon cv builder */}

      <Block
        // center
        // flex={false}
        margin={[sizes.getHeight(3), 0, 0, 0]}
        // height={sizes.getHeight(55)}
        // style={{borderWidth: 1, backgroundColor:'red'}}
      >
        <FlatList
          data={data}
          numColumns={status ? null : 2}
          key={!status ? 1 : 0}
          renderItem={({item}) => {
            return (
              <Button
                onPress={
                  () => 
                  getRelaventData(item.name)
                  // navigation.navigate(`${item.name}` )
                }
                center
                middle
                style={{
                  marginTop: sizes.getHeight(1.5),
                  marginLeft: sizes.getWidth(3),
                  backgroundColor: item.color,
                  borderRadius: sizes.withScreen(0.003),
                  height: sizes.getHeight(17),
                  width: sizes.getWidth(status ? 92.5 : 45),
                  // width: sizes.getWidth(92),
                }}
                // margin={[10,10,10,10]}
              >
                <Image
                  source={item.image}
                  style={{
                    tintColor: colors.primary,
                    resizeMode: 'contain',
                    flex: 0.25,
                    marginBottom: sizes.getHeight(1),
                  }}
                />
                <Text h3 color={colors.primary}>
                  {item.name}
                </Text>
              </Button>
            );
          }}
        />
      </Block>

      {/* Sign In  */}
      {!status && ( //if user is not logged in
        <Block
          center
          middle
          flex={false}
          // style={{borderWidth:1,backgroundColor:'pink'}}
          padding={[0, sizes.getWidth(3)]}
          // flex={false}
          height={sizes.getHeight(17)}>
          <SquareButton
            // onPress={() => navigation.navigate('Login')}
            onPress={() => navigation.navigate('Login')}
            withImage
            source={icons.login}
            bgColor={colors.lightPink}
            textColor={colors.customRed}
            height={sizes.getHeight(7)}>
            Log In
          </SquareButton>
          <SquareButton
            // onPress={() => navigation.navigate('Signup')}
            onPress={() => navigation.navigate('Signup')}
            withImage
            source={icons.white_user}
            bgColor={colors.customRed}
            textColor={colors.primary}
            height={sizes.getHeight(7)}>
            Sign Up
          </SquareButton>
        </Block>
      )}
      {/* <Block style={{position:'absolute', top:0, borderWidth:1,width:'100%'}}> */}
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={1}
      />
      {/* </Block> */}
      {isLoading && <ActivitySign />}
      {/* <ActivitySign /> */}
    </Block>
    // </ScrollView>
  );
};

export default HomePage;
