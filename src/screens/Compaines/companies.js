import React from 'react';
import {Block, Text, Searchbar, Button, CustomHeader} from 'components';
import {compainesList} from 'utils';
import {FlatList, Image} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import { useSelector } from 'react-redux';
import { companyLogo } from 'redux/apiConstants';


const CompainesList = ({navigation}) => {

  const {listOfCompanies} = useSelector(state => state.getLists)

  return (
    <Block>
      <CustomHeader bg={colors.brown} navigation={navigation} />
      <Searchbar title={"Companies"} placeholder={"Find for companies here"} />
      <Block padding={[0, sizes.getWidth(5)]}>
        <FlatList
            showsVerticalScrollIndicator={false}
            keyExtractor={(item, index) => {
              return index.toString();
            }}
          data={listOfCompanies}
          renderItem={({index,item}) => {
            console.log('====index')
            console.log(index)
            return (
              <Button
                // onPress={()=> navigation.navigate('AboutCompany')}
                center
                style={{
                  height: sizes.getHeight(10),
                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor:colors.gray,
                }}>
                <Block center middle flex={false} width={sizes.getWidth('20')}>
                  {/* <Image source={icons.qnbListLogo} style={{resizeMode:'contain', flex:0.3}} /> */}
                  <Image source={{uri:companyLogo+item.logo}} 
                    style={{resizeMode:'contain',width:sizes.getWidth(18), height:sizes.getHeight(7)}}
                   />
                  
                </Block>
                <Block flex={false} width={sizes.getWidth('65')}>
                  <Text h3 bold>
                    {item.name_en}
                  </Text>
                  <Block
                    margin={[sizes.getHeight(0.5),0,0,0]}
                    middle
                    padding={[0, 0, 0, sizes.getWidth(1)]}
                    flex={false}
                    width={sizes.getWidth(23)}
                    height={sizes.getHeight(3)}
                    style={{backgroundColor: colors.brown, borderRadius:sizes.withScreen(0.002)}}>
                    <Text
                      color={colors.primary}
                      style={{fontSize: sizes.customFont(10)}}>
                      {item.jobPosted} jobs posted
                    </Text>
                  </Block>
                </Block>
                <Image
                  source={icons.forward_arrow}
                  style={{width: sizes.getWidth(1.5), resizeMode: 'contain'}}
                />
              </Button>
            );
          }}
        />
      </Block>
    </Block>
  );
};

export default CompainesList;
