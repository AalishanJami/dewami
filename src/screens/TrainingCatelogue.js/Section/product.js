import React from 'react';
import {Block, Text, Button} from 'components';
import {Image} from 'react-native';
import {sizes, colors} from 'styles/theme';
import * as icons from 'assets/icons';
import StarRatings from './StarRating';
const Product = ({data,navigation}) => {
  const {name, price, image, rating, isLiked} = data;
  return (
    <Button
      onPress={()=>navigation.navigate('details')}
      flex={false}
      style={{
        borderRadius: sizes.withScreen(0.003),
        marginHorizontal: sizes.getWidth(2),
        marginTop: sizes.getHeight(3),
        // borderWidth: 1,
        width: sizes.getWidth(43),
        height: sizes.getHeight(35),
      }}>
      <Block flex={6} center>
        <Image source={image} style={{width: '100%', height: '100%'}} />
      </Block>
      <Button
        style={{
          position: 'absolute',
          // overflow:'hidden',
          right: sizes.getWidth(1),
          width: sizes.getWidth(6),
          height: sizes.getHeight(4),
          // borderWidth: 1
        }}>
        <Image
          source={icons.empty_heart}
          style={{tintColor: 'red', flex: 0.4, resizeMode: 'contain'}}
        />
      </Button>
      <Block flex={false}>
        <Text h4 bold>
          {name}
        </Text>
      </Block>
      <Block flex={false}>
        {/* <Text h4>Rating Stars</Text> */}
        <StarRatings />
      </Block>
      <Block flex={false}>
        <Text bold h4>
          QAR {price}
        </Text>
      </Block>
    </Button>
  );
};

export default Product;
