import React from 'react';
import {Block, Text, CustomHeader} from 'components';
import {Searchbar} from 'components';
import Product from './product';
import {FlatList} from 'react-native';
import {catalogueData as data} from 'utils';
import {sizes, colors} from 'styles/theme';
const TrainingCatelogue = ({navigation}) => {
  return (
    <Block center>
      <CustomHeader bg={colors.brown} navigation={navigation} />
      <Searchbar />
      <Block width={sizes.getWidth(95)} style={{backgroundColor: ''}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          numColumns={'2'}
          data={data}
          renderItem={({item}) => <Product data={item} navigation={navigation} />}
        />
      </Block>
    </Block>
  );
};

export default TrainingCatelogue;
