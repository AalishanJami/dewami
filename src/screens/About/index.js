import React from 'react';
import {Block, Text, CustomHeader} from 'components';
import {sizes, colors} from 'styles/theme';
import {Image} from 'react-native';
import * as images from 'assets/images';
import * as icons from 'assets/icons';

const About = ({navigation}) => {
  return (
    <Block>
      {/* <CustomHeader bg={colors.customRed} navigation={navigation} /> */}

      <Block
        flex={false}
        center
        middle
        style={{
          position: 'relative',
          width: '100%',
          // borderWidth:2,
          height: sizes.getHeight(25),
          top: -sizes.getHeight(10),
          overflow: 'hidden',
        }}>
        <Image source={images.aboutBack} style={{resizeMode: 'cover'}} />
      </Block>

      <Block
        flex={false}
        center
        middle
        style={{
          backgroundColor: colors.darkBrown,
          opacity: 0.7,
          position: 'absolute',
          width: '100%',
          height: sizes.getHeight(25),
          top: -sizes.getHeight(10),
          overflow: 'hidden',
        }}
      />
      <Block
        padding={[sizes.getWidth(5)]}
        style={{
            // borderWidth:2,
          marginTop: -sizes.getHeight(10),
        }}>
        <Text h2 bold>
          Your Future Job Gateway
        </Text>
        <Text h4 bold style={{lineHeight: 30}} color={colors.customRed}>
          Over 152m152m254 great job offers you deserve!
        </Text>
        <Text h3 style={{lineHeight: 22}}>
          There are many variations of passages of Lorem Ipsum available, but
          the majority have suffered alteration in some form, by injected
          humour, or randomised words which don't look even slightly believable.
          If you are going to use a passage of Lorem Ipsum, you need to be sure
        </Text>

        <Block
          center
          row
          flex={false}
          height={sizes.getHeight(10)}
          margin={[sizes.getHeight(2), 0, 0, 0]}
          //   style={{borderWidth: 1}}
        >
          <Block>
            <Image source={icons.briefCase} style={{resizeMode: 'contain'}} />
          </Block>
          <Block flex={false} width={sizes.getWidth(78)}>
            <Text h4 bold>
              Find Your Dream Job
            </Text>
            <Text h4>
              the majority have suffered alteration in some form, by injected
              humour, or randomised words which don't look even slightly
              believable
            </Text>
          </Block>
        </Block>
        <Block
          center
          row
          flex={false}
          height={sizes.getHeight(10)}
          //   style={{borderWidth: 1}}
        >
          <Block>
            <Image source={icons.briefCase} style={{resizeMode: 'contain'}} />
          </Block>
          <Block flex={false} width={sizes.getWidth(78)}>
            <Text h4 bold>
              Recruiter Profile
            </Text>
            <Text h4>
              the majority have suffered alteration in some form, by injected
              humour, or randomised words which don't look even slightly
              believable
            </Text>
          </Block>
        </Block>
        <Block
          center
          row
          flex={false}
          height={sizes.getHeight(10)}
          //   style={{borderWidth: 1}}
        >
          <Block>
            <Image source={icons.briefCase} style={{resizeMode: 'contain'}} />
          </Block>
          <Block flex={false} width={sizes.getWidth(78)}>
            <Text h4 bold>
              Advertise A Job
            </Text>
            <Text h4>
              the majority have suffered alteration in some form, by injected
              humour, or randomised words which don't look even slightly
              believable
            </Text>
          </Block>
        </Block>

        {/* end */}
      </Block>
    </Block>
  );
};

export default About;
