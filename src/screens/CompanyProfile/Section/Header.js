import React from 'react';
import {Block, Text, CustomHeader, Button} from 'components';
import {colors, sizes} from 'styles/theme';
import {Image, StyleSheet} from 'react-native';
import * as icons from 'assets/icons';

const PortfolioHeader = () => {
  return (
    <Block
      padding={[0, 0, 10, 0]}
      flex={2}
      style={{backgroundColor: colors.brown}}>
      {/* <CustomHeader /> */}
      <Block flex={3} center middle>
        <Image
          source={icons.qnbLogo}
          style={{resizeMode: 'contain', flex: 0.9}}
        />
      </Block>
      <Block bottom center middle margin={[0, 0, 10, 0]}>
        <Text h3 color={colors.primary}>
          QATAR NATIONAL BANK
        </Text>
      </Block>
      <Block row crossRight middle>
        <Button
          onPress={() => alert('Company Facebook Profile')}
          center
          middle
          style={styles.btnStyle}>
          <Image
            source={icons.facebookIcon}
            style={{resizeMode: 'contain', flex: 0.4}}
          />
        </Button>
        <Button
          onPress={() => alert('Company Twitter Profile')}
          center
          middle
          style={styles.btnStyle}>
          <Image source={icons.twitter} style={styles.logoStyle} />
        </Button>
        <Button
          onPress={() => alert('Company LinkedIn Profile')}
          center
          middle
          style={styles.btnStyle}>
          <Image source={icons.linkedin} style={styles.logoStyle} />
        </Button>
      </Block>
    </Block>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    borderWidth: 0.7,
    marginHorizontal: sizes.getWidth(1.5),
    borderColor: colors.primary,
    width: sizes.screenSize * 0.03,
    height: sizes.screenSize * 0.03,

    borderRadius: sizes.withScreen(0.07),
  },
  logoStyle: {
    resizeMode: 'contain',
    flex: 0.9,
    tintColor: colors.primary,
  },
});

export default PortfolioHeader;
