import React from 'react';
import {Block, Text,Button, ChatButton} from 'components';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import {Image} from 'react-native';
import {sizes, colors} from 'styles/theme';

const Addressbar = props => {
  const {image, text} = props;
  return (
    <Block
    center middle
      flex={false}
      height={sizes.getHeight(9)}
      style={{
        // borderWidth: 1,
        width: '100%',
        marginVertical: sizes.getHeight(0.7),
        borderRadius: sizes.withScreen(0.004),
        elevation: 10,
        backgroundColor: colors.primary,
      }}
      row
      center
      middle>
      <Block center middle margin={[0, sizes.getWidth(2), 0, 0]} flex={false}>
        <Image
          source={image}
          style={{
            tintColor: colors.gray3,
            resizeMode: 'contain',
            width: sizes.getWidth(3),
          }}
        />
      </Block>
      <Text style={{fontWeight: '900'}} h3 color={colors.gray3}>
        {text}
      </Text>
    </Block>
  );
};

const Contact = () => {
  return (
    <Block>
      <Block
        center
        middle
        flex={false}
        height={sizes.getHeight(20)}
        style={{overflow: 'hidden'}}>
        <Image
          source={images.map}
          style={{resizeMode: 'cover', width: '100%', height: '100%'}}
        />
      </Block>
      {/* Contact Info block started */}
      <Block
        flex={false}
        style={{
          // borderWidth: 1,
          position: 'absolute',
          top: sizes.getHeight(12),
          left: sizes.getWidth(7.5),
          zIndex: 10,
          width: sizes.getWidth(85),
          // height: sizes.getHeight(35),
          // backgroundColor: 'red',
        }}>
        <Block center>
          <Addressbar
            image={icons.location}
            text={'123 Al Shamal Road Gharafah, Doha QATAR'}
          />
          <Addressbar image={icons.phone} text={'+934567890'} />
          <Addressbar
            image={icons.message}
            text={'info@dawami.com'}
          />
        </Block>
      </Block>
      {/* message */}
    </Block>
  );
};

export default Contact;
