import About from './Section/About'
import Contact from './Section/Contact'
import Vacancies from './Section/Vacancies'
import Media from './Section/Media'
import Header from './Section/Header'
import CompanyPorfolio from './CompanyPortfolio'
import TabWrapper from './Section/TabNavigatorWrapper'
export default CompanyPorfolio
export {About, Contact,Vacancies, Media,Header,TabWrapper}