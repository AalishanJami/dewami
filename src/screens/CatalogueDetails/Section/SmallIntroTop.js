import React, {useState} from 'react';
import {Text, Block} from 'components';
import {AirbnbRating} from 'react-native-ratings';
import {sizes, colors} from 'styles/theme';

const SmallIntroTop = () => {
  const [rating, setRating] = useState(3);
  const ratingCompleted = rating => {
    console.log('Rating is: ' + rating);
    setRating(rating);
  };
  return (
    <Block middle flex={false} row margin={[0,0,sizes.getHeight(2.5),0]}>
      <Block flex={2}>
        <Text h4>130 Pages. Beginner. English / Spanish</Text>
      </Block>
      <Block row center rightacross>
      <Text color={'#f1c40f'} style={{ fontSize: sizes.customFont(10), marginRight: sizes.getWidth(1) }}>{rating}.0</Text>
        <AirbnbRating
          count={5}
          reviews={[1, 2, 3, 4, 5]}
          showRating={false}
          defaultRating={rating}
          starStyle={{margin: 1,overlayColor:'#f1c40f'}}
          size={11}
          reviewSize={25}
          onFinishRating={ratingCompleted}
        //   starContainerStyle={{}}
        />
        <Text bold style={{fontSize: sizes.customFont(8)}} color={colors.gray}>
          (1,234)
        </Text>
      </Block>
    </Block>
  );
};

export default SmallIntroTop;
