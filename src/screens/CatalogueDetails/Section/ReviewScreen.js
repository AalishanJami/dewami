import React, {useState} from 'react';
import {Block, Text} from 'components';
import {sizes, colors} from 'styles/theme';
import {AirbnbRating} from 'react-native-ratings';
import {Image} from 'react-native';
import * as images from 'assets/images';

const ReviewScreen = () => {
  const [rating, setRating] = useState(3);
  const ratingCompleted = rating => {
    console.log('Rating is: ' + rating);
    setRating(rating);
  };
  return (
    <Block 
    // style={{borderWidth: 1, height:sizes.getHeight(20)}}
    >
      <Text bold>Reviews</Text>
      <Block row center rightacross>
        <Text
          bold
          color={'#f1c40f'}
          style={{
            fontSize: sizes.customFont(10),
            marginRight: sizes.getWidth(1),
          }}>
          {rating}.0
        </Text>
        <AirbnbRating
          count={5}
          reviews={[1, 2, 3, 4, 5]}
          showRating={false}
          defaultRating={rating}
          starStyle={{margin: 1, overlayColor: '#f1c40f'}}
          size={11}
          onFinishRating={ratingCompleted}
          //   starContainerStyle={{}}
        />
        <Text style={{fontSize: sizes.customFont(8)}} color={colors.gray}>
          (1,234)
        </Text>
      </Block>
      {/* Details of user  */}
      <Block
      center
        margin={[sizes.getHeight(3), 0]}
        row
        flex={false}
        height={sizes.getHeight(9)}>
        <Block
          center
          middle
          flex={false}
          width={sizes.withScreen(0.05)}
          height={sizes.withScreen(0.05)}
          style={{borderRadius: sizes.withScreen(3), overflow: 'hidden'}}>
          <Image
            source={images.person}
            style={{resizeMode: 'contain', flex: 1}}
          />
        </Block>
        <Block padding={[0, 10]} middle flex={5} 
        // style={{borderWidth: 1}}
        >
          <Text h3 bold>
            Ibrahim Muhammad
          </Text>
          <Text h4 color={colors.gray}>UI/UX Lead</Text>
        </Block>
      </Block>
    </Block>
  );
};

export default ReviewScreen;
