import React, {useState} from 'react';
import {Block, Text, Button} from 'components';
import {Modal, Image, ScrollView} from 'react-native';
import {SmallIntroTop, Title, Description, ReviewScreen} from './Section';
import {sizes, colors} from 'styles/theme';
import * as images from 'assets/images';
import * as icons from 'assets/icons';
const CatalogueDetails = ({showDetails, navigation}) => {
  const [visible, setVisible] = useState(true);
  // const {showDetails} = props;
  return (
    <Block>
      <Modal
        onRequestClose={() => {{setVisible(false), navigation.navigate('Training Catalogue')}}}
        // visible={!showDetails && true}
        visible={visible}
        animationType="slide"
        >
        <Block center middle style={{borderWidth: 2}} flex={1}>
          <Image
            source={images.landscape}
            style={{width: '100%', height: '100%'}}
          />
          {/* Close */}
          <Button
            onPress={() => {
              setVisible(!visible);
              navigation.navigate('Training Catalogue')
            }}
            style={{
              position: 'absolute',
              top: sizes.getHeight(1),
              left: sizes.getWidth(2),
            }}>
            <Image
              source={icons.close_white}
              style={{resizeMode: 'contain', width: sizes.getWidth(4)}}
            />
          </Button>
          {/* HEART */}
          <Button
            style={{
              position: 'absolute',
              top: sizes.getHeight(1),
              right: sizes.getWidth(5),
            }}>
            <Image
              source={icons.empty_heart}
              style={{
                resizeMode: 'contain',
                tintColor: 'white',
                width: sizes.getWidth(4),
              }}
            />
          </Button>
        </Block>
        <Block
          margin={[10, 0]}
          padding={[0, sizes.getWidth(5)]}
          flex={2}
        //   style={{borderWidth: 1}}
          >
          <ScrollView
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}>
            <SmallIntroTop />
            <Title />
            <Description />
            <ReviewScreen />
          </ScrollView>
          <Block
            space={'between'}
            row
            middle
            padding={[0, sizes.getWidth(5)]}
            style={{position: 'absolute', bottom: sizes.getHeight(1)}}>
            <Button
              center
              middle
              style={{
                borderColor: '#DBDBDB',
                width: sizes.getWidth(43),
                borderWidth: 1,
              }}>
              <Text h2>Preview</Text>
            </Button>
            <Button
              center
              middle
              style={{
                marginHorizontal: 10,
                width: sizes.getWidth(43),
                backgroundColor: '#00e200',
              }}>
              <Text h2 color={colors.primary}>
                Add To Cart
              </Text>
            </Button>
          </Block>
        </Block>
      </Modal>
    </Block>
  );
};

export default CatalogueDetails;
