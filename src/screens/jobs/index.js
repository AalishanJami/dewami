import React, {useState, useEffect} from 'react';
import {Block, Text, Searchbar, Button, CustomHeader} from 'components';
import {vacancies} from 'utils';
import {FlatList, Image} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';
import {useSelector} from 'react-redux';
import {LANG_AR,} from 'redux/constants';
import {companyLogo} from 'redux/apiConstants'
const JobsList = ({navigation}) => {
  const {listOfJobs} = useSelector(state => state.getLists);
  const {currentLN} = useSelector(state => state.userInfo);
  console.log(currentLN);

  useEffect(() => {
    setTimeout(() => fetchListOfJobs, 3000);
  });

  const fetchListOfJobs = () => {};

  // console.log(listOfJobs)

  return (
    <Block>
      <CustomHeader bg={colors.darkBrown} navigation={navigation} />
      {/*  */}
      <Searchbar
        bg={colors.darkBrown}
        title={'Jobs'}
        placeholder={'Find for companies here'}
      />
      <Block padding={[0, sizes.getWidth(5)]}>
        <FlatList
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => {
            return index.toString();
          }}
          data={listOfJobs}
          renderItem={({item}) => {
            console.log(item.company.logo);
            return (
              <Button
                onPress={() => {
                  navigation.navigate('jobDetails');
                }}
                center
                style={{
                  height: sizes.getHeight(10),
                  flexDirection: 'row',
                  borderBottomWidth: 1,
                  borderBottomColor: colors.gray,
                }}>
                <Block
                  center
                  middle
                  flex={false}
                  width={sizes.getWidth('20')}
                  style={{borderWidth:0, height: '100%', marginRight: '3%'}}>
                  <Image
                    // source={icons.qnbListLogo}
                    // source={{uri:ImageBasePath + item.company.logo}}
                    source={{
                      uri:companyLogo+item.company.logo
                    }}
                    style={{resizeMode:'contain',width:sizes.getWidth(18), height:sizes.getHeight(7)}}
                  />
                </Block>
                <Block
                  padding={[4, 0, 0, 0]}
                  flex={false}
                  width={sizes.getWidth('65')}
                  style={{height: '70%'}}>
                  <Text h4 bold>
                    {/* {item.title} */}
                    {currentLN === LANG_AR ? item.title_ar : item.title_en}
                  </Text>
                  <Text h4 color={colors.gray2} style={{textAlign: 'left'}}>
                    {currentLN === LANG_AR
                      ? item.company.name_ar
                      : item.company.name_en}
                  </Text>
                  <Text h4 color={colors.gray2}>
                    {item.company.created_at.split(' ')[0]}
                  </Text>
                  {/* <Block
                    margin={[sizes.getHeight(0.5),0,0,0]}
                    middle
                    padding={[0, 0, 0, sizes.getWidth(1)]}
                    flex={false}
                    width={sizes.getWidth(20)}
                    height={sizes.getHeight(3)}
                    style={{backgroundColor: colors.brown}}>
                    <Text
                      color={colors.primary}
                      style={{fontSize: sizes.customFont(10)}}>
                      {item.jobPosted} jobs posted
                    </Text>
                  </Block> */}
                </Block>
                <Image
                  source={icons.forward_arrow}
                  style={{width: sizes.getWidth(1.5), resizeMode: 'contain'}}
                />
              </Button>
            );
          }}
        />
      </Block>
    </Block>
  );
};

export default JobsList;
