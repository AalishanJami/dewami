import React, {useState, useEffect} from 'react';
import {Block, Text, Button} from 'components';
import {StyleSheet, FlatList, PermissionsAndroid} from 'react-native';
import {sizes, colors} from 'styles/theme';
import {ActionButtons} from './buttonSection';
import RNFetchBlob from 'rn-fetch-blob';
import {ShowComments} from './showComments';
import {useSelector} from 'react-redux';


const DraftDetails = props => {
  // const {final} = useSelector(state => state.getLists.currentViewProg)
  const {
    id,
    request_id,
    name,
    description,
    created_at,
    file,
    data,
    final,
    // isWaiting,
    closeModal,
  } = props;

    console.log('....**props**....')
    console.log(props)

  // const [showComments, setShowComments] = useState(true);
  const disliked = (id, req_id) => {
    console.log(id);
    console.log(req_id);
  };
  const [isWaiting, setIsWaiting] = useState(false);
  const [height, setHeight] = useState();


  const likedHandler = async (draft_id, req_id) => {
    setIsWaiting(true);
    console.log(draft_id);
    console.log(req_id);
    //     const result = await draftLiked({req_id, draft_id}, err => {
    //       return messageRef.current?.show(
    //         <CustomAlert text="Can't Process Request. Please try again." />,
    //         3000,
    //       );
    //     });
    //     console.log('result');
    setIsWaiting(false);
  };

  const {drafts} = useSelector(state => state.getLists.currentViewProg);
  console.log('name');
  console.log(drafts);

  return (
    <Block center flex={false} height={height} style={styles.mainCon}>
      <Block
        row
        flex={false}
        //   style={{height: sizes.getHeight(10)}}
      >
        <Block style={{borderWidth: 0}}>
          <Text h2 color={colors.gray2}>
            Name
          </Text>
          <Text h2 color={colors.gray2}>
            Description
          </Text>
          <Text h2 color={colors.gray2}>
            Created At
          </Text>
        </Block>
        <Block flex={2} style={styles.detailsStyle}>
          <Text h2>{final && final.name ||  name}</Text>
          <Text h2 ellipsizeMode="tail" numberOfLines={1}>
            {final && final.description||description}
          </Text>
          <Text h2 style={{lineHeight: 27}}>
            {new Date(final && final.created_at || created_at).toLocaleDateString()}
          </Text>
        </Block>
      </Block>
      <ActionButtons {...props} />
    </Block>
  );
};

const styles = StyleSheet.create({
  mainCon: {
    overflow: 'hidden',
    backgroundColor: colors.primary,
    borderRadius: sizes.getWidth(2),
    // borderWidth: 0.1,
    // backgroundColor:'red',
    borderColor: 'red',
    elevation: 1,
    padding: sizes.padding,
    margin: sizes.getHeight(0.7),
    width: sizes.getWidth(90),
  },
  detailsStyle: {
    backgroundColor: '#EEEEEE99',
    borderTopLeftRadius: sizes.getWidth(2),
    borderTopRightRadius: sizes.getWidth(2),
    paddingHorizontal: sizes.padding * 1,
  },
});

export {DraftDetails};
