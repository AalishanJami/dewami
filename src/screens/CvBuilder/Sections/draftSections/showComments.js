import React, {useState, useRef} from 'react';
import {sizes, colors} from 'styles/theme';
import {Button, Block, Text} from 'components';
import {TextInput, StyleSheet} from 'react-native';

const ShowComments = props => {
  const {getComments,closeComments} = props;
  const [sendBtnDisable, setSendBtnDisabled] = useState(true);
  const [comments, setComments] = useState();

  const commentsHandler = e => {
    // setSendBtnDisabled(e.length > 0 ? true : false);
    setSendBtnDisabled(false);
    setComments(e);
  };
  const inputRef = useRef()
  const btnHandler = comments => {
    // console.log(comments)
    inputRef.current.clear()
    getComments(comments)
    setSendBtnDisabled(true)
    closeComments()
  }
  return (
    <Block
      center
      height={sizes.getHeight(25)}
      flex={false}
      style={{
        // borderWidth: 0.3,
        marginTop: sizes.getHeight(2),
        borderRadius: sizes.getWidth(2),
        width: '100%',
        // borderWidth:1,
        // backgroundColor: 'red',
      }}>
      <Block
        flex={3}
        style={{
          width: '100%',
        //   borderWidth: 0.3,
          borderStyle: 'dotted',
          borderRadius: sizes.getWidth(1),
        }}>
        <TextInput
          ref={inputRef}
          onChangeText={e => commentsHandler(e)}
          placeholder="Please Add Comments"
          multiline={true}
          numberOfLines={6}
          textAlignVertical="top"
          style={{
            paddingHorizontal: sizes.getWidth(3),
            width: '100%',
            // height: sizes.getHeight(15),
            borderWidth: 0.3,
          }}
        />
      </Block>
      <Block middle style={{borderWidth:0, width:'100%'}}>
        <Button
          onPress={() => btnHandler(comments) }
          disabled={sendBtnDisable}
          activeOpacity={0.3}
          center
          middle
          style={{
            ...styles.btnStyle,
            backgroundColor: sendBtnDisable ? colors.gray : colors.customRed,
          }}>
          <Text color={colors.primary}> Send</Text>
        </Button>
      </Block>
    </Block>

    // <Button activeOpacity={0.3} style={styles.btnStyle} onPress={()=>alert('1')}>
    //     <Text>Great</Text>
    // </Button>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    borderWidth: 0.5,
    borderColor: colors.gray,
    borderStyle: 'dashed',
    // backgroundColor: colors.gray,
    borderRadius: sizes.getWidth(1),
    width: '50%',
    alignSelf: 'flex-end',
    width: sizes.getWidth(20),
    //   backgroundColor:colors.customRed,
    marginVertical: sizes.getHeight(1),
  },
});

export {ShowComments};
