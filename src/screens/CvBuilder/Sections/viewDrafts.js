import React, {useState, useRef, useEffect} from 'react';
import {Block, Button, Text, ActivitySign, CustomAlert} from 'components';
import {sizes, colors} from 'styles/theme';
import {draftLiked} from 'redux/actions';
import {
  Image,
  FlatList,
  Platform,
  PermissionsAndroid,
  Alert,
  ActivityIndicator,
  StyleSheet,
  TextInput,
  Keyboard,
} from 'react-native';
import * as icons from 'assets/icons';
// import RNFS from 'react-native-fs'
import RNFetchBlob from 'rn-fetch-blob';
import Toast from 'react-native-easy-toast';
import {DraftDetails} from './draftSections/draftsDetails';
import {ActionButtons} from './draftSections/buttonSection';
import {useDispatch, useSelector} from 'react-redux';

const ViewDrafts = props => {
  const {data, closeModal} = props;
  const {currentViewProg} = useSelector(state => state.getLists);
  console.log('view Drafts Props');
  console.log(props.closeModal);

  console.log('currentViewProg.drafts------------')
  console.log(currentViewProg.drafts)

  // useEffect(() => {
  //   currentViewProg.dra
  // })

  return (
    <Block>
      {currentViewProg.drafts.map(el => {
        console.log('view draft-----------')
        console.log(el)
        return(
          <DraftDetails closeModal={closeModal} {...el} key={el.id} />
          )
        })}
    </Block>
  );
};

export {ViewDrafts};
