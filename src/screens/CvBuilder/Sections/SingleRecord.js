import React, {useEffect, useState} from 'react';
import {Block, Text, Button, ActivitySign} from 'components';
import {sizes, colors} from 'styles/theme';
import {FlatList} from 'react-native';
import {Image} from 'react-native-animatable';
import * as icons from 'assets/icons';
import {ViewDrafts} from './viewDrafts';
import {useSelector, useDispatch} from 'react-redux';
import Axios from 'axios';
import {ADDING_FEEDBACK} from 'redux/constants';
import {checkResponse} from 'redux/actions/getRequests';
import {DraftDetails} from './draftSections/draftsDetails';

const SingleRecord = props => {
  const {closeModal} = props;
  const {currentViewProg} = useSelector(state => state.getLists);
  const [isWaiting, setIsWaiting] = useState(false);
  const dispatch = useDispatch();

  // useEffect(() => {
  //   currentViewProg.drafts.forEach(async element => {
  //     setIsWaiting(true)
  //     const response = await checkResponse(element.id, err => {
  //       console.log('error is here ')
  //     })
  //     console.log('response===================')
  //     console.log(response)
  //     setIsWaiting(false)
  //     })
  // },[])

  let requestDetails = currentViewProg.final
    ? [currentViewProg.final]
    : currentViewProg.drafts;
  console.log(
    '------------------------------------currentViewProg---------------',
  );
  console.log(currentViewProg.final);
  return (
    <Block style={{backgroundColor: '#980025',  width: '100%'}} center middle>
      {currentViewProg.drafts.length ? (
        <Block center padding={[sizes.padding, 0]} style={{width: '100%'}}>
          <FlatList
            keyExtractor={index => {
              return index.toString();
            }}
            data={requestDetails}
            // data={data.final ? data.final  : data.drafts}
            renderItem={({item}) => {
              console.log(item);
              return (
                <Block
                  center
                  middle
                  style={{
                    height: item.length === 1 ? sizes.getHeight(90) : 'auto',
                  }}>
                  {currentViewProg.final && (
                    <Block>
                      <DraftDetails {...currentViewProg} />
                      {/* <DraftDetails /> */}
                    </Block>
                  )}
                  <ViewDrafts closeModal={closeModal} />
                </Block>
              );
            }}
          />
        </Block>
      ) : (
        <Block
          center
          middle
          flex={false}
          height={sizes.getHeight(20)}
          width={sizes.getWidth(90)}
          style={{
            backgroundColor: colors.customRed,
            borderRadius: sizes.getWidth(2),
            // borderWidth: 0.1,
            borderColor: 'red',
            elevation: 4,
          }}>
          <Text
            h3
            color={colors.primary}
            style={{textAlign: 'center', lineHeight: 25}}>
            No Draft Available Against This Request {'\n'}Please Wait ,{'\n'}
            Request is under observations.
          </Text>
        </Block>
        // <Text>No Length of</Text>
      )}

      {isWaiting && <ActivitySign />}
    </Block>
  );
};

export {SingleRecord};
