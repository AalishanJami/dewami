import React, {useState, useRef, useEffect, useCallback} from 'react';
import {
  Block,
  Text,
  Button,
  SquareButton,
  TextField,
  ActivitySign,
  CustomModal,
  PopupVerification,
  CustomAlert,
} from 'components';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Keyboard,
  Alert,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';
import {
  SignupFormState,
  validateField,
  validateFields,
  // fbHandler,
  // googleSigning,
} from 'utils';
import Toast from 'react-native-easy-toast';
import {GoogleSignin} from '@react-native-community/google-signin';
import {registerUser, checkUserNameAvailability} from 'redux/actions';
import {useDispatch, useSelector} from 'react-redux';
import {ADD_USER_INFO, REMOVE_USER_INFO} from 'redux/constants';
import {
  checkEmailAvailability,
  RemoveRegisteredAccount,
  removeUserInfo,
  VerifyUser,
} from 'redux/actions/auth';

export const Signup = ({navigation}) => {
  const alertTiming = 3000;
  const dispatch = useDispatch();
  const messageRef = useRef();
  const [state, setState] = useState({
    loginBtnDisable: false,
    acceptTerm: false,
    fields: {...SignupFormState},
  });

  const handleInput = ({name, text}) => {
    const newField = validateField(state.fields[name], text);
    setState({fields: {...state.fields, [name]: newField}});
  };

  // =========================================================================================================
  // =================== on Error Keyboard Position
  const setAlertToTop = sizes.getDimensions.height;
  const setAlertToBottom = sizes.getDimensions.height / 10;
  const [alertBar, setAlertBar] = useState(setAlertToBottom);
  // ================USERNAME INPUT HANDLER ======================
  const [nameIsAvailable, setNameisAvailable] = useState(false);
  const [isChecking, setIsChecking] = useState(false);
  const [currentStatusIcon, setCurrentStatusIcon] = useState(null);
  const [inputStatusBar, setInputStatusBar] = useState(null);
  // ========== EMAIL INPUT HANDLER =======================
  const [emailIsAvailable, setEmailIsAvailable] = useState(false);
  const [isCheckingEmail, setIsCheckingEmail] = useState(false);
  const [currentStatusIconEmail, setCurrentStatusIconEmail] = useState(null);
  const [inputStatusBarEmail, setInputStatusBarEmail] = useState(null);
  // ==========================================================================================================
  const [isLoading, setIsLoading] = useState(false);
  function toggleLoginButton() {
    setState(state => ({...state, loginBtnDisable: !state.loginBtnDisable}));
  }
  const {userInfo} = useSelector(state => state.userInfo);
  console.log(userInfo);
  const handleSubmit = async () => {
    // =================
    const result = validateFields(state.fields);
    const userDetails = {
      name: state.fields.name.value,
      email: state.fields.email.value,
      password: state.fields.password.value,
      confirmPassword: state.fields.confirmPassword.value,
      userName: state.fields.username.value,
    };
    if (result.validity) {
      setIsLoading(true);
      toggleLoginButton();
      if (state.fields.confirmPassword.value === state.fields.password.value) {
        if (state.acceptTerm) {
          // in case username or email availablity is under validation from api and user hits submit
          if (
            !isChecking &&
            currentStatusIconEmail === icons.tic &&
            currentStatusIcon === icons.tic
          ) {
            const reg_result = await registerUser(userDetails, err => {
              err && err.error
                ? (messageRef.current?.show(
                    <Text h4 color={colors.primary}>
                      {err.error}
                    </Text>,
                  ),
                  // toggleLoginButton(),
                  setIsLoading(false))
                : (messageRef.current?.show(
                   <CustomAlert text="Please check INTERNET CONNECTIVITY" />, alertTiming
                  ),
                  setIsLoading(false),
                  toggleLoginButton());
            });
            reg_result &&
              (dispatch({type: ADD_USER_INFO, payload: reg_result.user}),
              setIsLoading(true),
              toggleLoginButton(prev => !prev),
              setModalVisibility(true));
            // in case username or email availablity is under validation from api and user hits submit else block
          } else {
            messageRef.current?.show(
              <CustomAlert text="Please Wait.."/>,1500)
              toggleLoginButton()
          }
          // =========
        } else {
          messageRef.current?.show(
            <CustomAlert text={'Please check & accept term and condition box.'} />,alertTiming
          ),
            toggleLoginButton();
          setIsLoading(false);
        }
      } else {
        messageRef.current?.show(
          <CustomAlert text="Password doesn't match. Try Again" />,
          alertTiming,
        ),
          setIsLoading(false);
        toggleLoginButton();
        // }
      }
    } else {
      // if password doesn't match
      // if(state.fields.confirmPassword.value.length !== state.fields.password.value.length)
      if (userDetails.confirmPassword.length !== userDetails.password.length) {
        messageRef.current?.show(
         <CustomAlert text="Password Does not match." />,alertTiming
        ),
          setIsLoading(false);
      }
      toggleLoginButton();
      setIsLoading(false);
    }
    //finally
    setState(state => ({...state, fields: result.newFields}));
    setIsLoading(false);
  };

  useEffect(() => {
    _configureGoogleSignIn();
  });

  function _configureGoogleSignIn() {
    GoogleSignin.configure({
      offlineAccess: false,
    });
  }
  // ==============================================================
  const onFocusOut = async name => {
    console.log('i am launched');
    if (name === 'username') {
      if (state.fields.username.value !== '') {
        setIsChecking(true);
        const result = await checkUserNameAvailability(
          state.fields.username.value,
          err =>
            messageRef.current?.show(
              <CustomAlert text="Please Check INTERNET CONNECTIVITY" />,alertTiming ),
              setIsChecking(false),
              setCurrentStatusIcon(icons.warning),
            
        );
        // console.log('result')
        // console.log(result)
        result &&
          (result.success
            ? (console.log('i am in ok'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.tic),
              setNameisAvailable(true),
              setInputStatusBar(result.message))
            : (console.log('i am in not'),
              setIsChecking(false),
              setCurrentStatusIcon(icons.warning),
              setNameisAvailable(true),
              setInputStatusBar(result.message),
              toggleLoginButton()));
      } else {
        // if user type nothing
        // setIcon(null);
      }
    } else if (name === 'email') {
      if (state.fields.email.value !== '' && state.fields.email.isValid) {
        setIsCheckingEmail(true);
        const result = await checkEmailAvailability(
          state.fields.email.value,
          err =>
            messageRef.current?.show(
              <CustomAlert text="Please Check INTERNET CONNECTIVITY" />,alertTiming ),
              setIsCheckingEmail(false),
              toggleLoginButton(),
        );
        result &&
          (result.success
            ? (console.log('i am in ok email'),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.tic),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message))
            : (console.log('i am in not email '),
              setIsCheckingEmail(false),
              setCurrentStatusIconEmail(icons.warning),
              setEmailIsAvailable(true),
              setInputStatusBarEmail(result.message),
              toggleLoginButton()));
      } else {
        messageRef.current?.show(
          <CustomAlert text="Please wait.." />,alertTiming )
      }
    }
  };

  useEffect(() => {
    Keyboard.addListener('keyboardDidShow', () => {
      setAlertBar(setAlertToTop);
    });
    return () => {
      if (Keyboard.dismiss) {
        Keyboard.addListener('keyboardDidShow', () => {
          setAlertBar(setAlertToTop);
        });
      } else {
        Keyboard.addListener('keyboardDidHide'),
          () => {
            setAlertBar(setAlertToBottom);
          };
      }
    };
  });
  // pop up
  const [modalVisibility, setModalVisibility] = useState(false);
  // const [modalVisibility, setModalVisibility] = useState(true);
  const [codeStatus, setCodeStatus] = useState(false);
  const [modalIndicator, setModalIndicator] = useState(false);
  const [modalError, setModalError] = useState('');
  const nameRef = useRef();
  const usernameRef = useRef();
  const emailRef = useRef();
  const passRef = useRef();
  const conPassRef = useRef();
  const isValidCode = async status => {
    if (status) {
      // alert(status);
      console.log(userInfo)
      const result = await VerifyUser(userInfo, err => {
        console.log(err)
        alert(err)
      })
      result.success && (
      setModalVisibility(false),
      nameRef.current?.clear(),
        usernameRef.current?.clear(),
        emailRef.current?.clear(),
        passRef.current?.clear(),
        conPassRef.current?.clear(),
        setState(state => ({
          state,
          fields: SignupFormState,
          acceptTerm: false,
          loginBtnDisable: false,
        })),
        setCurrentStatusIcon(null),
        setInputStatusBar(null),
        setInputStatusBarEmail(null),
        setCurrentStatusIconEmail(null),
        navigation.navigate('Login', {registerStatus: result.success, message:result.message})
      )
    } else {
      setCodeStatus(true);
    }
  };
  const onBackPress = () => {
    // setModalVisibility(false)
    console.log(userInfo.email);
    Alert.alert('Final Warning', 'All Submitted record will be removed.', [
      // stay here
      {text: 'No, I wanted to continue', onPress: () => console.log(' Cancel')},
      // call api to remove
      {
        text: 'Yes, Remove',
        onPress: async () => {
          return (
            setModalIndicator(true),
            await RemoveRegisteredAccount(
              userInfo.email,
              err => (
                setModalError('Please Check your Internet'),
                console.log('error removing'),
                console.log(err),
                setModalIndicator(false)
              ),
            )
              .then(
                dispatch({
                  type: REMOVE_USER_INFO,
                  payload: {userInfo: null},
                }),
                setModalIndicator(false),
                setModalVisibility(false),
                nameRef.current?.clear(),
                usernameRef.current?.clear(),
                emailRef.current?.clear(),
                passRef.current?.clear(),
                conPassRef.current?.clear(),
                setState(state => ({
                  state,
                  fields: SignupFormState,
                  acceptTerm: false,
                  loginBtnDisable: false,
                })),
                setCurrentStatusIcon(null),
                setInputStatusBar(null),
                setInputStatusBarEmail(null),
                setCurrentStatusIconEmail(null),
                console.log(state),
              )
              .catch(e => {
                console.log('CATCH STATE IN IF CANCEL');
                alert(e);
                setModalIndicator(false);
              })
          );
        },
      },
    ]);
  };

  return (
    <Block>
      {/* Image */}
      <Block flex={false} height={sizes.getHeight(25)} center middle>
        <Image
          source={icons.red_logo}
          style={{resizeMode: 'contain', flex: 0.6}}
        />
      </Block>

      <ScrollView>
        {/* Inputs */}
        <Block padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}>
          <TextField
            inputRef={nameRef}
            onChangeText={handleInput}
            name={'name'}
            inputStyling={styles.inputStyle}
            placeholder={'Type A Name'}
          />

          {!state.fields.password.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.name.errorMessage}
            </Text>
          )}
          {/* =========================================USERNAME============================================== */}
          <Block center flex={false} row>
            <TextField
              inputRef={usernameRef}
              onFocus={() => {
                setCurrentStatusIcon(null), setInputStatusBar(null);
              }}
              onBlur={() => onFocusOut('username')}
              onChangeText={handleInput}
              name={'username'}
              inputStyling={styles.inputStyle}
              placeholder={'Choose A Username'}
            />
            {nameIsAvailable && (
              <Image
                source={currentStatusIcon}
                style={{
                  marginHorizontal: sizes.border,
                  position: 'absolute',
                  right: 0,
                  resizeMode: 'contain',
                  width: sizes.getWidth(4),
                  tintColor:
                    (currentStatusIcon === icons.warning && colors.customRed) ||
                    (currentStatusIcon === icons.tic && 'green') ||
                    'black',
                }}
              />
            )}
            {isChecking && (
              <ActivityIndicator
                size="small"
                color={colors.customRed}
                style={{position: 'absolute', right: sizes.getWidth(2)}}
              />
            )}
          </Block>
          {inputStatusBar !== null && (
            <Text
              h4
              color={
                currentStatusIcon === icons.warning ? colors.customRed : 'green'
              }>
              {inputStatusBar}
            </Text>
          )}
          {!state.fields.password.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.username.errorMessage}
            </Text>
          )}
          {/* =========================================Email============================================== */}
          <Block center flex={false} row>
            <TextField
              inputRef={emailRef}
              onFocus={() => {
                setCurrentStatusIconEmail(null), setInputStatusBarEmail(null);
              }}
              onBlur={() => onFocusOut('email')}
              onChangeText={handleInput}
              name={'email'}
              inputStyling={styles.inputStyle}
              autoCompleteType={'email'}
              placeholder={'Type Email'}
              keyboardType="email-address"
            />
            {emailIsAvailable && (
              <Image
                source={currentStatusIconEmail}
                style={{
                  marginHorizontal: sizes.border,
                  position: 'absolute',
                  right: 0,
                  resizeMode: 'contain',
                  width: sizes.getWidth(4),
                  tintColor:
                    (currentStatusIconEmail === icons.warning &&
                      colors.customRed) ||
                    (currentStatusIconEmail === icons.tic && 'green') ||
                    'black',
                }}
              />
            )}
            {isCheckingEmail && (
              <ActivityIndicator
                size="small"
                color={colors.customRed}
                style={{position: 'absolute', right: sizes.getWidth(2)}}
              />
            )}
          </Block>
          {inputStatusBarEmail !== null && (
            <Text
              h4
              color={
                currentStatusIconEmail === icons.warning
                  ? colors.customRed
                  : 'green'
              }>
              {inputStatusBarEmail}
            </Text>
          )}
          {!state.fields.email.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.email.errorMessage}
            </Text>
          )}

          {/* =========================================Password============================================== */}
          <TextField
            inputRef={passRef}
            secureTextEntry={true}
            onChangeText={handleInput}
            name={'password'}
            inputStyling={styles.inputStyle}
            autoCompleteType={'password'}
            placeholder={'Password'}
          />
          {!state.fields.password.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.password.errorMessage}
            </Text>
          )}
          <TextField
            inputRef={conPassRef}
            secureTextEntry={true}
            onChangeText={handleInput}
            name={'confirmPassword'}
            inputStyling={styles.inputStyle}
            placeholder={'Confirm Password'}
          />
          {!state.fields.password.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.password.errorMessage}
            </Text>
          )}
        </Block>

        {/* remember Me */}
        <Block
          padding={[0, sizes.getWidth(6), 0, sizes.getWidth(6)]}
          center
          middle
          flex={false}>
          <SquareButton
            disabled={state.loginBtnDisable}
            onPress={handleSubmit}
            bgColor={state.loginBtnDisable ? colors.gray : colors.customRed}
            textColor={colors.primary}>
            Sign Up
          </SquareButton>
        </Block>

        <Block
          margin={[sizes.getHeight(3), 0, 0, 0]}
          padding={[0, sizes.getWidth(5), 0, sizes.getWidth(5)]}
          row
          style={{justifyContent: 'space-between'}}>
          <Button row center>
            <CheckBox
              // tintColors ={[colors.brown,colors.customRed]}
              // onChangeValue={e => alert('value')}
              value={state.acceptTerm}
              onChange={() => {
                setState(state => ({...state, acceptTerm: !state.acceptTerm}));
              }}
            />
            <Text h2 color={colors.gray2}>
              I accept the terms & conditions
            </Text>
          </Button>
        </Block>

        {/* Social Login */}
        <Block
          margin={[sizes.getHeight(3), 0, 0, 0]}
          padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}
          flex={4}>
          <Block center>
            <SquareButton
              onPress={() => navigation.navigate('Login')}
              noBorder
              width={'90%'}>
              <Text h2 color={colors.gray}>
                Already have an account? Log In
              </Text>
            </SquareButton>
          </Block>
          {/* ============================ */}
        </Block>
      </ScrollView>
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 0,
        }}
        // positionValue={sizes.getDimensions.height /10}
        positionValue={alertBar}
        fadeInDuration={100}
        fadeOutDuration={300}
        opacity={1}
      />

      <CustomModal closeModal={onBackPress} isVisible={modalVisibility}>
        <PopupVerification
          errorCode={modalError}
          indicatorStatus={modalIndicator}
          codeStatus={codeStatus}
          isValid={isValidCode}
          compareWithCode={`${userInfo?.confirmation_code}`}
        />
      </CustomModal>

      {isLoading && <ActivitySign />}
    </Block>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderBottomColor: colors.gray,
    borderBottomWidth: 0.7,
    width: '100%',
    paddingRight: sizes.getWidth(10),
    // borderWidth:2,
  },
});
