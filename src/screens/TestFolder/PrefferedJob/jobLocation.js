import React from 'react';
import {Block} from 'components';
import {VisaSelection} from './visaSelection';
import {CountrySelection} from './countrySelection';
import {AddBtn} from '../AddBtn';
import {sizes, colors} from 'styles/theme';

const JobLocation = props => {
  const {countryValue, country,visaValues, SelectedVisa} = props
  console.log('=======JOB LOCATION PROPS==========');
  console.log(props);
  console.log('===================================');

  return (
    <Block
      padding={[0, sizes.getWidth(2)]}
      flex={false}
      margin={[sizes.getHeight(2), 0, 0, 0]}>
      <Block row flex={false} height={sizes.getHeight(13)}>
        {/* Options */}
        {/* <Block padding={[0, sizes.getWidth(3)]} flex={2}>
          <CountrySelection country={country} countryValue={countryValue} />
          <VisaSelection visaValues={visaValues} selectedVisa={SelectedVisa} />
        </Block> */}

        {/* Buttons */}
        {/* <Block middle>
          <AddBtn />
        </Block> */}
      </Block>
    </Block>
  );
};

export {JobLocation};
