import React from 'react';
import {Block} from 'components';
import {Picker} from '@react-native-community/picker';
import {StyleSheet} from 'react-native';
import {sizes} from 'styles/theme';

const CountrySelection = props => {
  const {country, countryValue} = props;

  const getLabel = value => {
    switch (value) {
      case '1':
        return 'Pakistan';
      case '2':
        return 'Qatar';

      default:
        break;
    }
  };
  return (
    <Block middle height={sizes.getHeight(7)}>
      <Picker selectedValue={countryValue} style={styles.pickerStyle}>
        {country.length &&
          country.map((val, index) => {
            return (
              <Picker.Item
                color="white"
                key={index}
                value={val}
                label={getLabel(val)}
              />
            );
          })}
      </Picker>
    </Block>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    height: sizes.getHeight(5),
    backgroundColor: '#FF673F',
  },
});

export {CountrySelection};
