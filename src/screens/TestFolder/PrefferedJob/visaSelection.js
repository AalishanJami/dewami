import React from 'react';
import {Block} from 'components';
import {Picker} from '@react-native-community/picker';
import {StyleSheet} from 'react-native';
import { sizes } from 'styles/theme';

const VisaSelection = () => {
  return (
    <Block middle height={sizes.getHeight(7)}>
      <Picker style={styles.pickerStyle}>
        <Picker.Item label={'aa'} value={''} />
        <Picker.Item label={'cc'} value={''} />
        <Picker.Item label={'ff'} value={''} />
        <Picker.Item label={'gg'} value={''} />
      </Picker>
    </Block>
  );
};

const styles = StyleSheet.create({
  pickerStyle: {
    height: sizes.getHeight(5),
    backgroundColor:'gray'
  },
});

export {VisaSelection};
