import React from 'react';
import { Block, Button, Text } from 'components';
import { StyleSheet } from 'react-native';
import { sizes } from 'styles/theme';

const AddBtn = props => {
  return (
      <Block flex={false} center middle style={styles.blkStyle}>
        <Button activeOpacity={0.3} center middle style={styles.btnStyle}>
          <Text>Add</Text>
        </Button>
      </Block>
     
  );
};

const styles = StyleSheet.create({
    btnStyle:{
        width:'100%',
        height:'90%',
        borderWidth: 1,
        borderRadius:sizes.getWidth(1)
    },
    blkStyle:{
      marginHorizontal:sizes.getWidth(1)

  },
})

export {AddBtn};
