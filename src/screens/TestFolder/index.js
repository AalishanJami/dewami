import React, {useEffect, useState, useReducer} from 'react';
import {Block, Text, Button, ActivitySign} from 'components';
import {AddBtn} from './AddBtn';
import {RemoveBtn} from './removeBtn';
import {SubmitBtn} from './submitBtn';
import {StyleSheet, ActivityIndicator} from 'react-native';
import {sizes} from 'styles/theme';
import {GetProfileInfo} from 'redux/actions';
import {useSelector, useDispatch} from 'react-redux';
import { JobLocation } from './PrefferedJob/jobLocation'

const PreferJob = () => {
  const dispatch = useDispatch();
  const userBasicProfile = useSelector(state => state.auth.userBasicProfile);
  const userData = useSelector(state =>state.userInfo.userData)
  const [isWaiting, setIsWaiting] = useState(false);
  useEffect(() => {
    async function fetchProfileRecords() {
    setIsWaiting(true);
      const result = await GetProfileInfo(userBasicProfile.id, err => {
        return console.log(err), setIsWaiting(false);
      });
      result && (dispatch(result), setIsWaiting(false));
    }
    if (!userData.success) {
      fetchProfileRecords();
    }
  }, []);


//   console.log(userData.profile.pereferred_job.title)

  const initialState = {
    // disableSaveBtn: true,
    // title: userInfo.profile.pereferred_job.title || [],
    // jobCategory: userInfo.profile.pereferred_job.category_id,
    // jobLocation: userInfo.profile.pereferred_job.country || ['1'],
    // jobLevel: userInfo.profile.pereferred_job.level,
    // jobIndustry: userInfo.profile.pereferred_job.industry_id,
    // targetSalary: userInfo.profile.pereferred_job.target_salary,
    // targetCurrency: userInfo.profile.pereferred_job.target_currency,
    // summary: userInfo.profile.pereferred_job.summary,
    // jobType: userInfo.profile.pereferred_job.type,
    // visaType: userInfo.profile.pereferred_job.visa || ['1'],
    // noticePeriod: userInfo.profile.pereferred_job.notice_period,
  };
  const reducer = (state,action) => {
      return state
  }
 
  const [state,localDispatch]=useReducer(reducer,initialState)
//   console.log(state)

  return (
    <Block  center style={styles.blkStyle}>
      <Block flex={2} width={sizes.getDimensions.width}>
        

        <Text> Data </Text>
        {/* <JobLocation  {...state} /> */}
        {/* {country.map((value,index)=>{
            return(
                <JobLocation key={index} countryValue={value} {...state} />
            )
        })}  */}
      </Block>

     

      {isWaiting && <ActivitySign />}
    </Block>
  );
};

const styles = StyleSheet.create({
    blkStyle:{
        width:'100%',
    }
});

export {PreferJob};
