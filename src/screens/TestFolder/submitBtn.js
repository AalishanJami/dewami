import React from 'react';
import {Block, Button, Text} from 'components';
import {StyleSheet} from 'react-native';
import {sizes} from 'styles/theme';

const SubmitBtn = () => {
  return (
    <Block center middle style={styles.blkStyle}>
      <Button activeOpacity={0.3} center middle style={styles.btnStyle}>
        <Text>Submit</Text>
      </Button>
    </Block>
  );
};

const styles = StyleSheet.create({
  btnStyle: {
    borderWidth: 1,
    width: '100%',
    borderRadius: sizes.getWidth(1),
  },
  blkStyle:{
    marginHorizontal:sizes.getWidth(1)

},
});

export {SubmitBtn};
