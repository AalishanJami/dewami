import ShowAll from './Section/ShowAll'
import Responsibilities from './Section/Responsibilities'
import AboutQatarGass from './Section/About'
import EduWithExp from './Section/EduWithExp'
import OtherBenifits from './Section/Benefits'
import JobDescription from './JobDescription'
import Header from './Section/Header'
import TabWrapper from './Section/TabNavigatorWrapper'
export default JobDescription
export {ShowAll,Responsibilities,AboutQatarGass,EduWithExp,OtherBenifits,Header,TabWrapper}