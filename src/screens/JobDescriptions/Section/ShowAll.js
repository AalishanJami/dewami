import React from 'react';
import {Block, Text} from 'components';
import {Image, ScrollView} from 'react-native';
import * as icons from 'assets/icons';
import * as images from 'assets/images';
import {sizes, colors} from 'styles/theme';
import AboutQatarGass from './About';
import  OtherBenifits from './Benefits'
import  EduWithExp from './EduWithExp'
import  Responsibilities from './Responsibilities'
const ShowAll = () => {
  return (
    <Block>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        <Block
        margin={[sizes.getHeight(2),0,0,0]}
        padding={[sizes.getWidth(3), sizes.getWidth(5), 0, sizes.getWidth(5)]}>
          <Text h4 bold>
            Job Description
          </Text>
          <Text
            padding={[10, 0]}
            style={{fontSize: sizes.customFont(10), lineHeight: 18}}>
            I have been waiting. I’ve been waiting all day.
            {'\n'}
            Waiting for Gus to send one of his men to kill me. And it’s you. Who
            do you know, who’s okay with using children, Jesse? Who do you know…
            who’s allowed children to be murdered… hmm? Gus! He has, he has been
            ten steps ahead of me at every turn.
          </Text>
        </Block>
        <Responsibilities />
        <EduWithExp />
        <OtherBenifits />
        <AboutQatarGass />
      
      </ScrollView>
    </Block>
  );
};

export default ShowAll;
