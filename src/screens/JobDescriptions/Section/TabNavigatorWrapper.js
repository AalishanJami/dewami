import React, {useState} from 'react';
import {Block, Text} from 'components';
import {TabView, SceneMap, TabBar, ScrollPager} from 'react-native-tab-view';
import {Dimensions} from 'react-native';
// import {About, Vacancies, Media, Contact} from 'screens/CompanyProfile';
import {
  ShowAll,
  Responsibilities,
  AboutQatarGass,
  EduWithExp,
  OtherBenifits,
} from 'screens/JobDescriptions';
import {colors, sizes} from 'styles/theme';

const TabNavigatorWrapper = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = useState([
    {key: 'showall', title: 'Show All'},
    {key: 'res', title: 'Responsibilities'},
    {key: 'edu', title: 'Education & Experience'},
    {key: 'benifits', title: 'Other Benifits'},
    {key: 'about', title: 'About Qatar Gass'},
  ]);
  const initialLayout = {width: Dimensions.get('window').width};

  const renderScene = SceneMap({
    showall: () => <ShowAll />,
    res: () => <Responsibilities />,
    edu: () => <EduWithExp />,
    benifits: () => <OtherBenifits />,
    about: () => <AboutQatarGass />,
  });

  const renderTabBar = props => (
    <TabBar
      {...props}
      scrollEnabled={true}
      indicatorStyle={{backgroundColor: colors.brown}}
      style={{
        backgroundColor: 'transparent',
        elevation: 0,
        borderBottomColor: colors.darkBrown,
        borderBottomWidth:0.9,
      }}
      indicatorStyle={{
        borderWidth: 2.2,
        borderColor: colors.darkBrown,
        marginLeft:sizes.getWidth(2),
        borderTopLeftRadius: sizes.getWidth(10),
        borderTopRightRadius: sizes.getWidth(10),
      }}
      activeColor={colors.darkBrown}
      inactiveColor={colors.gray2}
      labelStyle={{
        textTransform: 'capitalize',
        fontSize: sizes.withScreen(0.01),
        fontWeight: 'bold',
      }}
      tabStyle={{
        height: sizes.getHeight(5.5),
        width: 'auto',
        padding: 0,
        paddingHorizontal: sizes.getWidth(1),
        marginLeft:sizes.getWidth(2),
      }}
    />
  );

  return (
    <TabView
      navigationState={{index, routes}}
      renderScene={renderScene}
      onIndexChange={setIndex}
      // initialLayout={()=><Contact />}
      renderTabBar={renderTabBar}
    />
  );
};

export default TabNavigatorWrapper;
