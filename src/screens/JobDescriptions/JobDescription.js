import React from 'react';
import {Block, Text, CustomHeader} from 'components';
import {Header, TabWrapper} from 'screens/JobDescriptions';
import {sizes, colors} from 'styles/theme';
const JobDesciption = ({navigation}) => {
  return (
    <Block>
      <CustomHeader bg={colors.darkBrown} navigation={navigation} />
      <Header />
      <Block flex={5}>
        <TabWrapper />
      </Block>
    </Block>
  );
};

export default JobDesciption;
