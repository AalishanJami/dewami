import React, {useState, useEffect, createRef, useRef} from 'react';
import {
  Block,
  Text,
  Button,
  SquareButton,
  TextField,
  SimpleToast,
  ActivitySign,
  CustomAlert,
} from 'components';
import {
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
  Keyboard,
} from 'react-native';
import CheckBox from '@react-native-community/checkbox';
import * as icons from 'assets/icons';
import {colors, sizes} from 'styles/theme';
import {LoginFormState, validateField, validateFields} from 'utils';
import Toast from 'react-native-easy-toast';
import {fbHandler, googleSigning} from 'utils';
import {GoogleSignin,statusCodes} from '@react-native-community/google-signin';
import {useSelector, useDispatch, batch} from 'react-redux';
import {loginUser, saveUser, goWithgoogle} from 'redux/actions/auth';
import {USER_LOGIN, ADD_USER_INFO} from 'redux/constants';
import {goWithFacebook, GetProfileInfo} from 'redux/actions';
// ========================CODE STARTEd=================

export const Login = ({route, navigation}) => {
  const alertTiming = 3000
  const [state, setState] = useState({
    loginBtnDisable: false,
    fields: {...LoginFormState},
  });
  const [isLoading, setIsLoading] = useState(false);
  const [checkbox, setCheckbox] = useState(false);
  // useSelector(state=> console.log(state))
  const messageRef = useRef();
  useEffect(() => {
    GoogleSignin.configure({
      offlineAccess: false,
      // webClientId:'206246080380-8rev8p06u07f1mm8e1u4s24k4an8g0s3.apps.googleusercontent.com'
      webClientId:'206246080380-lf8jg6q039gova27eph82gbh4983k4q9.apps.googleusercontent.com'
    });

  })
  const handleInput = ({text, name}) => {
    const newField = validateField(state.fields[name], text);
    setState({fields: {...state.fields, [name]: newField}});
  };
  function toggleLoginButton() {
    setState(state => ({...state, loginBtnDisable: !state.loginBtnDisable}));
  }
  const dispatch = useDispatch();

  // ==================================LOGIN WITH FACEBOOK========================================
  const loginWithFacebook = async () => {
    try {
      setIsLoading(true);
      const userInfo = await fbHandler();
      if (userInfo === null) {
        messageRef.current?.show(
          <CustomAlert text={'Please check INTERNET CONNECTIVITY..'} />,
        );
      }
      // User Founded but
      else {
        // check if email of user is not available
        if (!userInfo.email) {
          messageRef.current?.show(
            <CustomAlert text="No Email detected with this Facebook Account.." />,
            alertTiming,
          );
        }
        // if email found with this facebook account ---- proceeded to owm API
        else {
          const result = await goWithFacebook(userInfo, err => {
            // FACEBOOK ERROR FROM OWNED API WILL BE HERE--------
            setIsLoading(false);
          });
          result &&
            batch(() => {
              dispatch({
                type: USER_LOGIN,
                payload: {success: result.payload.success},
              });
              dispatch({type: ADD_USER_INFO, payload: result.payload.user});
            });
          setIsLoading(false);
        }
      }
    } catch (e) {
      // console.log(e);
      messageRef.current?.show(
        <CustomAlert text={"Request Aborted."} />,
        alertTiming,
      )
      setIsLoading(false);
    }
    setIsLoading(false);
  }
  // ============================GOOGE LOGIN========================================////////
  const loginWithGoogle = async () => {
    // messageRef.current?.show(<CustomAlert text={"Google Service is Unavailable yet. Please wait for next update."} />,alertTiming)
    setIsLoading(true);
    try {
      const user = await googleSigning();


      if (user.name) {
        console.log('**user');
        console.log(user);
        const result = await goWithgoogle(user, err => {
          messageRef.current?.show(<CustomAlert text={err} />);
          setIsLoading(false);
        });
        result &&
          (batch(() => {
            dispatch({
              type: USER_LOGIN,
              payload: {success: result.payload.success},
            }),
              dispatch({type: ADD_USER_INFO, payload: result.payload.user});
          }),
          setIsLoading(false))
          await GoogleSignin.revokeAccess()
      } else {
        messageRef.current?.show(
          <CustomAlert
            text={
              'No USERNAME associated with GOOGLE ACCOUNT \nPlease Try Again..'
            }
          />,
          2500,
        );
        setIsLoading(false),
        await GoogleSignin.revokeAccess()
      }
    } catch (e) {
      messageRef.current?.show(
        <CustomAlert text={"We couldn't process your request right now.."} />,
        alertTiming,
      );
      setIsLoading(false),
      await GoogleSignin.revokeAccess()
    }
  };
  // ===============================HANDLE SUBMIT=======================
  const handleSubmit = async () => {
    Keyboard.dismiss();
    const result = validateFields(state.fields);
    const {
      fields: {email, password},
    } = state;
    if (result.validity) {
      setIsLoading(true);
      toggleLoginButton();
      const login_result = await loginUser(
        {email: email.value, password: password.value},
        err => {
          if (err) {
            if (err.error) {
              messageRef.current?.show(
                <CustomAlert text={err.error} />,
                alertTiming,
              ),
                setIsLoading(false);
            } else {
              messageRef.current?.show(
                <CustomAlert text="Check your INTERNET CONNECTIVITY" />,
                alertTiming,
              );
              setIsLoading(false);
            }
          }
          setIsLoading(false);
        },
      );
      if (login_result) {
        console.log(login_result)
        const profileInfo = await GetProfileInfo(login_result.payload.user.id, err => {
          console.log('cant find profile and unable to add in redux')
          messageRef.current?.show(
            <CustomAlert text="Due to Some Error, We couldn't get your info, please contact Application owner. " />,
            alertTiming,
          );
          setIsLoading(false);
        })
        // console.log('----------ADD PROFILE INFO')
        // console.log(profileInfo)
        // const savingUserInfo = saveUser(login_result.payload.user)
        const savingUserInfo = saveUser(profileInfo.payload.user)
        // console.log(savingUserInfo)
        batch(() => {
          dispatch(login_result);
          dispatch(savingUserInfo);
          profileInfo && dispatch(profileInfo)
        }),
          toggleLoginButton(),
          setIsLoading(false);
        // navigation.navigate('Home');
      }
      // IF VALIDITIY
    } else {
      // when user put something fishy
      console.log('ERROR ');
      messageRef.current?.show(
        <CustomAlert text="Please Follow Instructions" />,
        alertTiming,
      );
      toggleLoginButton();
      setIsLoading(false);
    }
    setState(state => ({...state, fields: result.newFields}));
    setIsLoading(false);
  };

  // ========================================================================
  useEffect(() => {
    setState(state => ({
      ...state,
      fields: LoginFormState,
      loginBtnDisable: false,
    }));
  }, [navigation]);
  // ========================================================================
  const passwordRef = useRef();
  const EmailRef = useRef();
  const [navParams, setNavParams] = useState(route.params);

  useEffect(() => {
    passwordRef.current?.clear(), EmailRef.current?.clear();
    setState(state => ({
      ...state,
      fields: LoginFormState,
      loginBtnDisable: false,
    }));
    // console.log('i am coming fro mregistration please set popup here ');
    navParams?.registerStatus &&
      (messageRef.current?.show(
        <CustomAlert text="Registration Successful. Please Login" />,
        alertTiming,
      ),
      setNavParams(route.params));
    return () => {
      const statusbar = route.params;
      setNavParams(statusbar);
      navParams?.resetCompleted &&
        (passwordRef.current?.clear(),
        EmailRef.current?.clear(),
        setState(state => ({
          ...state,
          fields: LoginFormState,
          loginBtnDisable: false,
        })));
      // if user is coming from register and get successed
      // console.log('REGISTRATION STATUS');
      // console.log(navParams);
      // navParams?.registerStatus &&
      //   (messageRef.current?.show(
      //     <CustomAlert text="Registration Successful. Please Login" />,
      //     alertTiming,
      //   ),
      //   setNavParams(route.params));
    };
  }, [route.params, navigation]);
  // =====================================

  // function _configureGoogleSignIn() {
  //   GoogleSignin.configure({
  //     offlineAccess: false,
  //   });
  // }
  return (
    <Block>
      {/* Image */}
      <Block flex={false} height={sizes.getHeight(25)} center middle>
        <Image
          source={icons.red_logo}
          style={{resizeMode: 'contain', flex: 0.6}}
        />
      </Block>
      {/* {state.loginBtnDisable && (
        <ActivityIndicator size="large" color={colors.customRed} />
      )} */}

      <ScrollView
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}>
        {/* Inputs */}
        <Block
          flex={false}
          // height={sizes.getHeight(25)}
          padding={[0, sizes.getWidth(7)]}
          // style={{borderWidth:1}}
        >
          <TextField
            inputRef={EmailRef}
            onChangeText={handleInput}
            name={'email'}
            inputStyling={styles.inputStyle}
            autoCompleteType={'email'}
            keyboardType={'email-address'}
            placeholder={'Email'}
          />
          {!state.fields.email.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.email.errorMessage}
            </Text>
          )}
          <TextField
            inputRef={passwordRef}
            secureTextEntry={true}
            onChangeText={handleInput}
            name={'password'}
            inputStyling={styles.inputStyle}
            autoCompleteType={'password'}
            placeholder={'Password'}
          />
          {!state.fields.password.isValid && (
            <Text h4 color={colors.customRed}>
              {state.fields.password.errorMessage}
            </Text>
          )}
          {/* remember Me */}
          <Block
            padding={[0, sizes.getWidth(6), 0, sizes.getWidth(6)]}
            center
            middle
            // style={{borderWidth:1}}
            flex={false}>
            <SquareButton
              disabled={state.loginBtnDisable}
              onPress={handleSubmit}
              // onPress={() => navigation.navigate('Home')}
              bgColor={state.loginBtnDisable ? colors.gray : colors.lightPink}
              textColor={colors.customRed}>
              Log In
            </SquareButton>
          </Block>
        </Block>

        <Block
          margin={[sizes.getHeight(3), 0, 0, 0]}
          padding={[0, sizes.getWidth(5), 0, sizes.getWidth(5)]}
          row
          style={{justifyContent: 'space-between'}}>
          <Button row center>
            <CheckBox
              value={checkbox}
              onChange={() => setCheckbox(!checkbox)}
            />
            <Text h2 color={colors.gray2}>
              Remember Me
            </Text>
          </Button>
          <Button
            onPress={() => navigation.navigate('ForgetPassword')}
            center
            middle>
            <Text h2 color={colors.gray2}>
              Forget Password?
            </Text>
          </Button>
        </Block>

        {/* Social Login */}
        <Block
          margin={[sizes.getHeight(3), 0, 0, 0]}
          padding={[0, sizes.getWidth(7), 0, sizes.getWidth(7)]}
          // style={{borderWidth:1}}
          flex={4}>
          {/* or connet with */}
          <Block>
            <Text h2 color={colors.gray2}>
              Or connect with
            </Text>
          </Block>
          {/* social buttons */}
          <Block
            middle
            margin={[sizes.getHeight(2), 0, 0, 0]}
            row
            style={{justifyContent: 'space-between'}}>
            {/* ========================== */}
            <SquareButton
              onPress={loginWithFacebook}
              height={sizes.getHeight('7')}
              withImage
              source={icons.facebook}
              style={{justifyContent: 'center', alignItems: 'center'}}
              width={'46%'}>
              <Text h2 bold>
                Facebook
              </Text>
            </SquareButton>
            <SquareButton
              onPress={loginWithGoogle}
              height={sizes.getHeight('7')}
              withImage
              source={icons.google}
              style={{justifyContent: 'center', alignItems: 'center'}}
              width={'46%'}>
              <Text h2 bold>
                Google
              </Text>
            </SquareButton>
          </Block>
          {/* ========================== */}
          <Block center>
            <SquareButton
              onPress={() => (
                navigation.navigate('Signup'),
                passwordRef.current?.clear(),
                EmailRef.current?.clear(),
                setState(state => ({
                  ...state,
                  fields: LoginFormState,
                  // loginBtnDisable: false,
                }))
              )}
              noBorder
              width={'90%'}>
              <Text h2 color={colors.gray}>
                Don't have an account? Sign Up
              </Text>
            </SquareButton>
          </Block>
          {/* ============================ */}
        </Block>
      </ScrollView>
      <Toast
        ref={messageRef}
        style={{
          backgroundColor: colors.customRed,
          width: sizes.getWidth(100),
          borderRadius: 2,
        }}
        positionValue={sizes.getDimensions.height}
        fadeInDuration={200}
        fadeOutDuration={100}
        opacity={0.8}
      />
      {isLoading && <ActivitySign />}
    </Block>
  );
};

const styles = StyleSheet.create({
  inputStyle: {
    borderBottomColor: colors.gray,
    borderBottomWidth: 2,
    width: '100%',
    // borderWidth:1
    marginBottom: sizes.getHeight(2),
  },
});
