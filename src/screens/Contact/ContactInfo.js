import React from 'react'
import { Text, Block } from 'components'
import {Image} from 'react-native';
import * as icons from 'assets/icons';
import {sizes, colors} from 'styles/theme';


const ContactInfo = () => {
    return (
        <Block
        flex={false}
        padding={[0, sizes.getWidth(5)]}
        style={{
          //   borderWidth: 1,
          backgroundColor: '#AB7264',
          height: sizes.getHeight(28),
        }}>
        <Text h2 bold color={colors.primary}>
          Get In Touch
        </Text>
        <Text h3 style={{marginTop: sizes.getHeight(2)}} color={colors.primary}>
          There are many variations of passage of Lorem Ipsum avaiable, but the
          majority have suffered alteration in some form, by inject humour
        </Text>
        <Block row center flex={false} style={{marginTop: sizes.getHeight(1)}}>
          <Image
            source={icons.location}
            style={{
              resizeMode: 'contain',
              marginRight: sizes.getWidth(2),
              tintColor: colors.primary,
              width: sizes.getWidth(4),
            }}
          />
          <Text color={colors.primary} h3>
            123 - Al Shamal Road, Ghaffar Doha,Qatar
          </Text>
        </Block>

        <Block
          row
          center
          flex={false}
          style={{
            // borderWidth: 1,
            height: sizes.getHeight(4),
            marginTop: sizes.getHeight(1),
          }}>
          <Block center row>
            <Image
              source={icons.message}
              style={{
                resizeMode: 'contain',
                marginRight: sizes.getWidth(2),
                tintColor: colors.primary,
                width: sizes.getWidth(4),
              }}
            />
            <Text color={colors.primary} h3>
              info@dawami.com
            </Text>
          </Block>
          <Block>
            <Block center row bottom>
              <Image
                source={icons.phone}
                style={{
                  resizeMode: 'contain',
                  marginRight: sizes.getWidth(2),
                  tintColor: colors.primary,
                  width: sizes.getWidth(4),
                }}
              />
              <Text color={colors.primary} h3>
                +7412345987
              </Text>
            </Block>
          </Block>
        </Block>
      </Block>
    )
}

export default ContactInfo
