import React from 'react'
import { StyleSheet, Image } from 'react-native';
import { sizes, colors } from 'styles/theme';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import {DrawerItem} from '@react-navigation/drawer';

const DrawerItems = ({label,image,route,to,labelStyle}) => {
    return (
        <DrawerItem
        onPress={()=>alert('1')}
        labelStyle={styles.labelStyle}
        style={styles.listStyle}
        icon={({color, size}) => (
          <Image source={image} style={styles.iconStyle} />
        )}
        label={label}
        onPress={() => {route.navigate(to)}}
      />
    )
}






const styles = StyleSheet.create({
    labelStyle: {
      // borderWidth: 1,
      marginHorizontal: -sizes.getHeight(3),
      fontSize: sizes.h2,
    },
    listStyle: {
      // borderWidth: 1,
      height: sizes.getHeight(5),
      justifyContent: 'center',
    },
    iconStyle: {
      tintColor: colors.gray3,
      width: sizes.getWidth(4),
      resizeMode: 'contain',
    },
  });
  

export default DrawerItems