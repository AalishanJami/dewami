import React from 'react'
import { Block, Button, Text } from 'components'
import { sizes, colors } from 'styles/theme'
import { Image } from 'react-native'
import * as images from 'assets/images';
import * as icons from 'assets/icons';
import { useSelector } from 'react-redux';

const DrawerInfo = (props) => {
  const {userInfo} = useSelector(state=>state.userInfo)

    return (
        <Block margin={[0,0,sizes.getHeight(5),0]} flex={false} height={sizes.getHeight(25)}>
          <Block top crossRight padding={[sizes.getHeight(1), 0]}>
            <Button>
              <Text color={'green'}>AR</Text>
            </Button>
          </Block>
          <Block
            center
            flex={2}
            padding={[0, sizes.getWidth(1)]}
            row
            style={{backgroundColor: 'white', elevation: 1}}>
            <Block
              center
              middle
              flex={false}
              style={{
                // borderWidth:0.6,
                // borderColor:colors.gray,
                marginRight:sizes.getWidth(3),
                width:sizes.screenSize*0.06,
                height: sizes.screenSize*0.06,
                borderRadius:sizes.screenSize*0.1,
                overflow:'hidden'
              }}>
              <Button
                center
                middle
                style={{
                  height:'100%',
                  width: '100%',
                }}>
                {/* <Image source={images.person} style={{resizeMode: 'contain', flex:1}} /> */}
                <Image source={icons.avatarDummy} style={{resizeMode:'contain', flex:1, tintColor:colors.gray }} />

                {/* <Image source={{uri:userInfo.avatar}} style={{resizeMode: 'contain', flex:1}} /> */}
              </Button>
            </Block>
            <Block flex={2}>
              <Text h3 bold style={{textTransform: 'capitalize'}}>
                {/* {userInfo.user.username || "NO USER NAME FOUND"} */}
              </Text>
              {/* <Text h3>Web Developer</Text> */}
              <Text 
                style={{fontSize:userInfo.user.expertise ? sizes.h3 : sizes.h4}}
              >{userInfo.user.expertise || "Please Add Your Expertise"}</Text>
            </Block>
          </Block>
        </Block>
    )
}

export default DrawerInfo
