import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import { Profile, EditProfile, Chat, Favorites,Messages } from 'screens';

const UserStack = () => {
  const UserStack = createStackNavigator();
  return (
    <UserStack.Navigator>
      <UserStack.Screen
        name="Profile"
        component={Profile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <UserStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{
          header: props => (
            <CustomHeader backBtn bg={colors.customRed} {...props} />
          ),
        }}
      />
      <UserStack.Screen
        name="Messages"
        component={Messages}
        options={{
          header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <UserStack.Screen
        name="Chat"
        component={Chat}
        options={{
          header: props => <CustomHeader bg={colors.darkBrown} {...props} />,
        }}
      />
      <UserStack.Screen
        name="Favorites"
        component={Favorites}
        options={{
          header: props => <CustomHeader backBtn {...props} />,
        }}
      />
    </UserStack.Navigator>
  );
};

export {UserStack};
