// import React from 'react';
// import {createStackNavigator} from '@react-navigation/stack';
// import {
//   WelcomeScreen,
//   WelcomingScreen,
//   ForgetPassword,
//   Login,
//   Signup,
//   HomePage,
// } from 'screens';
// import {CustomHeader} from 'components';
// import {colors} from 'styles/theme';
// import { useSelector } from 'react-redux';


// const AuthStack = () => {
//   const {status} = useSelector(state => state.auth)
//   const AuthNavigation = createStackNavigator();
//   return (
   
//     <AuthNavigation.Navigator initialRouteName={'Home'}>
//     {/* <AuthNavigation.Navigator initialRouteName={'Login'}> */}
//       <AuthNavigation.Screen
//         name="Home"
//         component={HomePage}
//         options={{
//           header: props => (
//             <CustomHeader
//               bg={colors.primary}
//               tint={colors.customRed}
//               {...props}
//             />
//           ),
//         }}
//       />
//       <AuthNavigation.Screen
//         name="Welcome"
//         component={WelcomeScreen}
//         options={{headerShown: false}}
//       />
//       <AuthNavigation.Screen
//         name="AgainWelcome"
//         component={WelcomingScreen}
//         options={{headerShown: false}}
//       />
//       <AuthNavigation.Screen
//         name="ForgetPassword"
//         component={ForgetPassword}
//         options={{
//           header: props => (
//             <CustomHeader bg={colors.customRed} backBtn {...props} />
//           ),
//         }}
//       />
//       <AuthNavigation.Screen
//         name="Login"
//         component={Login}
//         options={{headerShown: false}}
//       />
//       <AuthNavigation.Screen
//         name="Signup"
//         component={Signup}
//         options={{headerShown: false}}
//       />
//     </AuthNavigation.Navigator>
//   );
// };

// export {AuthStack};
