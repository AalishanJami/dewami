import React, {useContext, useEffect} from 'react';
import {View, Text} from 'react-native';
import {
  Login,
  Signup,
  WelcomeScreen,
  WelcomingScreen,
  HomePage,
  TrainingCatalogue,
  Companies,
  CatalogueDetails,
  CompanyPorfolio,
  JobDescription,
  JobList,
  Profile,
  EditProfile,
  Messages,
  Contact,
  About,
  ForgetPassword,
  Favorites,
  Chat,
} from 'screens';
import {CustomHeader} from 'components';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {colors} from 'styles/theme';
import DrawerContent from './DrawerContent';
import {AppStack, AuthStack, UserStack} from 'navigations';
import {useSelector} from 'react-redux';

export const Navigations = () => {


  const authSection = useSelector(state => state.auth);
  const {status} = authSection;
  //if auth success then navigate to Home

  const Drawer = createDrawerNavigator();
  const Stack = createStackNavigator();

  const AppStackWithDrawer = (props) => {
    return (
      <Drawer.Navigator drawerContent={props => <DrawerContent {...props} /> }>
          <Drawer.Screen name="app" component={AppStack} {...props} />
      </Drawer.Navigator>
    );
  };


 

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none" >
        {status ? 
        <Stack.Screen name="app" component={AppStackWithDrawer} /> 
        :
        <Stack.Screen name="auth" component={AppStack} />
        }
      </Stack.Navigator>

    </NavigationContainer>
  );
};



{/* <Drawer.Navigator drawerContent={props => <DrawerContent {...props} />}> */}
